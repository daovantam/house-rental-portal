const withSass = require('@zeit/next-sass')
const withImages = require('next-images')
const Dotenv = require("dotenv-webpack");
const withFonts = require("next-fonts");

module.exports = withFonts(withImages(withSass(
  {
    serverRuntimeConfig: {
      onServer: true
    },
    webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
      // Add the new plugin to the existing webpack plugins
      config.plugins.push(new Dotenv({ silent: true }));
      return config;
    }
  },
),
))