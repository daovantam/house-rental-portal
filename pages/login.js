import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Router, { withRouter } from 'next/router'
import Layout from '../components/layout'
import Head from 'next/head'
import { Form, Input, Button, Checkbox, Modal } from 'antd'
import { UserOutlined, LockOutlined, MailOutlined } from '@ant-design/icons'
import Link from 'next/link'
import AuthsActions from '../redux/_auths-redux'
import { Auth } from '../constants'
import { css } from "@emotion/core";
import Spinner from '../components/spinner'


class Login extends Component {
  state = {
    loading: true,
    visibleForgotPassword: false
  };

  login = values => {
    let formData = new FormData()
    formData.append('username', values.username)
    formData.append('password', values.password)
    formData.append('grant_type', Auth.GRANT_TYPE)
    this.props.login(formData)
  }

  componentDidUpdate(prevProps) {
    if (prevProps.data !== this.props.data) {
      let data = this.props.data
      if (data) {
        Router.push('/')
      }
    }
  }

  openModalForgotPassword = () => {
    this.setState({
      visibleForgotPassword: true,
    });
  }

  handleForgotPassword = (values) => {
    console.log(values)
    this.props.resetPassword(values.username_forgot)
  };

  handleCancelForgotPassword = e => {
    console.log(e);
    this.setState({
      visibleForgotPassword: false,
    });
  };

  render() {
    return (
      <Layout {...this.props}>
        <Head>
          <title>{'Login'}</title>
        </Head>
        {this.props.processing && <Spinner />}
        <div className="login_container">
          <div className="login_container__content">
            <div className="text-center">
              <h1 className="title">Đăng nhập</h1>
            </div>
            <Form
              name="normal_login"
              className="login_container__content__form"
              initialValues={{
                remember: true,
              }}
              onFinish={this.login}
            >
              <Form.Item
                name="username"
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng nhập tên người dùng!',
                  },
                ]}
              >
                <Input size="large" prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Tên người dùng" />
              </Form.Item>
              <Form.Item
                name="password"
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng nhập mật khẩu! ',
                  },
                ]}
              >
                <Input.Password
                  size="large"
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  type="password"
                  placeholder="Mật khẩu"
                />
              </Form.Item>
              <Form.Item>
                <p className="login_container__content__form--forgot text-links" onClick={this.openModalForgotPassword}>
                  Quên mật khẩu?
                </p>
              </Form.Item>

              <Form.Item>
                <Button size="large" type="primary" htmlType="submit" className="login_container__content__form--button">
                  Đăng nhập
                </Button>
                <span>Bạn chưa có tài khoản?</span>
                <Link href={`/register`}>
                  <a> Đăng ký ngay</a>
                </Link>
              </Form.Item>
            </Form>
          </div>

          {/* modal forgot password */}
          <Modal
            title="Nhập tên người dùng để lấy lại mật khẩu!"
            visible={this.state.visibleForgotPassword}
            // onOk={this.handleForgotPassword}
            onCancel={this.handleCancelForgotPassword}
            footer={null}
          >
            <Form
              name="normal_login"
              className="login_container__content__form"
              initialValues={{
                remember: true,
              }}
              onFinish={this.handleForgotPassword}
            >
              <Form.Item
                name="username_forgot"
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng nhập tên người dùng!',
                  },
                ]}
              >
                <Input size="large" prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Tên người dùng" />
              </Form.Item>
              <Form.Item>
                <div className="d-flex-center-end">
                  <Button size="large" type="primary" htmlType="submit" className="login_container__content__form--button" style={{ width: '30%' }}>
                    Gửi
                  </Button>
                </div>
              </Form.Item>
            </Form>
          </Modal>
        </div>

      </Layout>
    )
  }
}

Login.propTypes = {
  history: PropTypes.object,
  login: PropTypes.func,
  processing: PropTypes.bool,
  data: PropTypes.object,
  error: PropTypes.object,
  shows: PropTypes.array,
  noFooter: PropTypes.bool
}

const mapStateToProps = state => {
  return {
    processing: state.auths.processing,
    succeed: state.auths.succeed,
    data: state.auths.data,
    error: state.auths.error
  }
}

const mapDispatchToProps = dispatch => ({
  login: data => dispatch(AuthsActions.loginRequest(data)),
  resetPassword: data => dispatch(AuthsActions.resetPasswordRequest(data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Login))
