import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Router,{ withRouter } from 'next/router'
import Layout from '../components/layout'
import Head from 'next/head'
import AuthsActions from '../redux/_auths-redux'
import { Form, Input, Button } from 'antd';
import { UserOutlined, LockOutlined, MailOutlined, PhoneOutlined, IdcardOutlined } from '@ant-design/icons';
import Link from 'next/link'
import { Regext } from '../constants'
import Spinner from '../components/spinner'

const emailRegex = /^[a-z][a-z0-9_\.]{5,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$/;
class Register extends Component {

  register = values => {
    let params = {
      email: values.email,
      username: values.username,
      password: values.password,
      phone: values.phoneNumber,
      identityNo: values.identityNo,
    }
    this.props.register({
      params, callback: (value) => {
        if (value) {
          Router.push("/login");
        } else {
        }
      }
    });
  }

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue("password")) {
      callback("Hai mật khẩu không khớp!");
    } else {
      callback();
    }
  };

  render() {
    return (
      <Layout {...this.props}>
        <Head>
          <title>{'Register'}</title>
        </Head>
        {this.props.processing && <Spinner />}
        <div className="login_container">
          <div className="login_container__content">
            <div className="text-center">
              <h1 className="title">Đăng ký</h1>
            </div>
            <Form
              name="normal_login"
              className="login_container__content__form"
              initialValues={{
                remember: true,
              }}
              onFinish={this.register}
            >
              <Form.Item
                name="email"
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng nhập email!',
                  },
                  {
                    pattern: Regext.EMAIL_REGEX,
                    message: "Vui lòng nhập đúng định dạng email",
                  }
                ]}
              >
                <Input size="large" prefix={<MailOutlined className="site-form-item-icon" />} placeholder="Email" />
              </Form.Item>
              <Form.Item
                name="username"
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng nhập tên người dùng!',
                  },
                  {
                    min: 6,
                    message: "Tên người dùng phải chứa tối thiểu 6 kí tự!",
                  },
                  {
                    max: 32,
                    message: "Tên người dùng không quá 32 kí tự!",
                  },
                ]}
              >
                <Input size="large" prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Tên người dùng" />
              </Form.Item>
              <Form.Item
                name="password"
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng nhập mật khẩu!',
                  },
                  // {
                  //   pattern: Regext.PASSWORD_REGEX,
                  //   message:
                  //     "Mật khẩu phải dài từ 8-20 kí tự, chứa số, kí tự đặc biệt, chữ thường và in hoa",
                  // },
                ]}
              >
                <Input.Password
                  size="large"
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  type="password"
                  placeholder="Mật khẩu"
                />
              </Form.Item>

              <Form.Item
                name="confirm"
                rules={[
                  {
                    required: true,
                    message: 'Xác nhận lại mật khẩu!',
                  },
                  ({ getFieldValue }) => ({
                    validator(rule, value) {
                      if (!value || getFieldValue('password') === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject('Hai mật khẩu không khớp!');
                    },
                  }),
                ]}
              >
                <Input.Password
                  size="large"
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  type="password"
                  placeholder="Xác nhận mật khẩu"
                />
              </Form.Item>

              <Form.Item
                name="phoneNumber"
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng nhập số điện thoại!',
                  },
                  {
                    pattern: Regext.PHONE_REGEX,
                    message: "Vui lòng nhập đúng định dạng số điện thoại",
                  }
                ]}
              >
                <Input size="large" prefix={<PhoneOutlined className="site-form-item-icon" />} placeholder="Số điện thoại" />
              </Form.Item>

              <Form.Item
                name="identityNo"
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng nhập số  thẻ căn cước/CMTND!',
                  }
                ]}
              >
                <Input size="large" prefix={<IdcardOutlined className="site-form-item-icon" />} placeholder="Thẻ căn cước/CMTND" />
              </Form.Item>

              <Form.Item>
                <Button size="large" type="primary" htmlType="submit" className="login_container__content__form--button">
                  Đăng ký
                </Button>
                <span>Bạn đã có tài khoản?</span>
                <Link href={`/login`}>
                  <a> Đăng nhập ngay</a>
                </Link>
              </Form.Item>
            </Form>
          </div>
        </div>

      </Layout>
    )
  }
}

Register.propTypes = {
  history: PropTypes.object,
  login: PropTypes.func,
  processing: PropTypes.bool,
  data: PropTypes.object,
  error: PropTypes.object,
  shows: PropTypes.array,
  noFooter: PropTypes.bool
}

const mapStateToProps = state => {
  return {
    processing: state.auths.processing,
    data: state.auths.data,
    error: state.auths.error
  }
}

const mapDispatchToProps = dispatch => ({
  register: data => dispatch(AuthsActions.registerRequest(data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Register))
