import React, { Component } from 'react'
import Layout from '../../components/layout'
import { connect } from 'react-redux'
import { withRouter } from 'next/router'
import Head from 'next/head'
import HouseActions from '../../redux/_house-redux'
import Link from 'next/link'
import i18n from '../../i18n';
import PaginationComponent from '../../components/pagination'
import RoomList from '../../components/room/room-list'
import Spinner from '../../components/spinner'

class Room extends Component {

  onSelectPageChange = (page) => {
    let params = {
      direction: true,
      matching: [
      ],
      orderBy: "updatedTimestamp",
      page: page - 1,
      pageSize: 12
    }
    this.props.getListHouse(params)
    window.scrollTo(0, 0)
  };

  componentDidMount() {
    let params = {
      direction: true,
      matching: [],
      orderBy: "updatedTimestamp",
      page: 0,
      pageSize: 12
    }
    this.props.getListHouse(params)
  }

  render() {
    const listHouse = this.props.searchData?.list_object
    const total_element = this.props.searchData?.total_element
    const page_size = this.props.searchData?.page_size
    console.log(this.props.searchData, total_element);
    return (
      <Layout>
        <Head>
          <title>Danh sách phòng</title>
        </Head>
        {/* {this.props.processing && <Spinner />} */}
        <div className="container-content">
          <div className="mt-5">
            <h3 className="fw-7 mb-0">Danh sách phòng </h3>
            <p className="fs-16">Thông tin cập nhật thường xuyên, chi tiết, chính xác</p>
          </div>
          <RoomList roomList={listHouse} />
          <div>
            <PaginationComponent onChange={this.onSelectPageChange} totalItem={total_element} pageSize={page_size} />
          </div>
        </div>
      </Layout>
    )
  }
}
const mapStateToProps = state => {
  return {
    processing: state.houses.processing,
    searchData: state.houses.searchData,
    error: state.houses.error
  }
}

const mapDispatchToProps = dispatch => ({
  getListHouse: data => dispatch(HouseActions.searchRequest(data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Room))

