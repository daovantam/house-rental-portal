import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'next/router'
import Layout from '../../components/layout'
import Head from 'next/head'
import { Slick } from '../../constants'
import Slider from "react-slick"
import { PhoneOutlined, UserOutlined, InfoCircleOutlined, ReconciliationOutlined, WifiOutlined, TagsOutlined, EditOutlined } from '@ant-design/icons';
import { LightgalleryProvider, LightgalleryItem } from 'react-lightgallery'
import classnames from 'classnames'
import Spinner from '../../components/spinner'
import AppUtils from '../../utils/AppUtils'
import HouseActions from '../../redux/_house-redux'
class RoomDetail extends Component {
  static async getInitialProps(ctx) {
    console.log(ctx)
    let pageProps = {}
    ctx.store.dispatch(HouseActions.getDetailHouseRequest(ctx.query.id))
    // // if (Component.getInitialProps) {
    //   pageProps = await Component.getInitialProps(ctx)
    // // }
    console.log(pageProps)
    return {
      pageProps
    }
  }
  state = {
    showDescFade: false,
    showSeeMore: false,
    showMore: false
  }

  checkDesHeight = () => {
    console.log("vao dya")
    if (document.getElementById('room-des')) {
      let descHeight = document.getElementById('room-des').clientHeight
      console.log(descHeight)
      if (descHeight >= 300) {
        this.setState({
          showSeeMore: true,
          showDescFade: true
        })
      }
    }
  }
  componentDidMount() {
    this.checkDesHeight()

  }

  handleShowMore = () => {
    this.setState({
      showMore: !this.state.showMore,
      showDescFade: !this.state.showDescFade
    })
  }

  render() {

    const { showDescFade, showMore, showSeeMore } = this.state;
    const data = this.props.detailData
    return (
      <Layout {...this.props}>
        <Head>
          <title>{'Room Detail'}</title>
        </Head>
        {this.props.processing && <Spinner />}
        <div className="room_detail">
          <div className="room_detail__header">
            <LightgalleryProvider
              lightgallerySettings={
                {
                  download: false,
                }
              }
            >
              <Slider {...Slick.SLIDE_DETAIL}>
                <div className="room_detail__header__img">
                  <LightgalleryItem src='https://spaceshare.s3.amazonaws.com/media/space_photos/Toong_HDT_21_result.jpg' group="space">
                    <img src='https://spaceshare.s3.amazonaws.com/media/space_photos/Toong_HDT_21_result.jpg' />
                  </LightgalleryItem>
                </div>
                <div className="room_detail__header__img">
                  <LightgalleryItem src='https://spaceshare.s3.amazonaws.com/media/space_photos/Toong_HDT_21_result.jpg' group="space">
                    <img src='https://spaceshare.s3.amazonaws.com/media/space_photos/Toong_HDT_21_result.jpg' />
                  </LightgalleryItem>
                </div>
                <div className="room_detail__header__img">
                  <LightgalleryItem src='https://spaceshare.s3.amazonaws.com/media/space_photos/Toong_HDT_21_result.jpg' group="space">
                    <img src='https://spaceshare.s3.amazonaws.com/media/space_photos/Toong_HDT_21_result.jpg' />
                  </LightgalleryItem>
                </div>
                <div className="room_detail__header__img">
                  <LightgalleryItem src='https://spaceshare.s3.amazonaws.com/media/space_photos/Toong_HDT_21_result.jpg' group="space">
                    <img src='https://spaceshare.s3.amazonaws.com/media/space_photos/Toong_HDT_21_result.jpg' />
                  </LightgalleryItem>
                </div>
                <div className="room_detail__header__img">
                  <LightgalleryItem src='https://spaceshare.s3.amazonaws.com/media/space_photos/Toong_HDT_21_result.jpg' group="space">
                    <img src='https://spaceshare.s3.amazonaws.com/media/space_photos/Toong_HDT_21_result.jpg' />
                  </LightgalleryItem>
                </div>
              </Slider>
            </LightgalleryProvider>
          </div>
          <div className="container-detail room_detail__content d-flex mt-5">
            <div className="room_detail__content__left">
              <div className="room_detail__content__room_name">
                <h2>{data.title}</h2>
              </div>
              <div className="room_detail__content__info_title">
                <InfoCircleOutlined className="room_detail__content__detail_icon--info" />
                <span className="mr-3">Thông tin phòng</span>
              </div>
              <div className="room_detail__content__info_detail mt-3">
                <div className="mb-3"><TagsOutlined className="fs-18 mr-2" /><span className="fs-18 fw-7">{data.categoryName}</span></div>
                <ul className="room_detail__content__info_detail__list">
                  <li>
                    <div className="d-flex-column">
                      <span className="fs-16 fw-7 text-color">Giá phòng</span>
                      <span>{AppUtils.number_format(data.price)} đồng</span>
                    </div>
                  </li>
                  <li>
                    <div className="d-flex-column">
                      <span className="fs-16 fw-7 text-color">Diện tích</span>
                      <span>{AppUtils.number_format(data.acreage)}m2</span>
                    </div>
                  </li>
                  <li>
                    <div className="d-flex-column">
                      <span className="fs-16 fw-7 text-color">Đặt cọc</span>
                      <span>{AppUtils.number_format(20000)} đồng</span>
                    </div>
                  </li>
                  <li>
                    <div className="d-flex-column">
                      <span className="fs-16 fw-7 text-color">Số phòng ngủ</span>
                      <span>{data.numberOfBedroom} phòng</span>
                    </div>
                  </li>
                  <li>
                    <div className="d-flex-column">
                      <span className="fs-16 fw-7 text-color">Điện</span>
                      <span>{AppUtils.number_format(3000)} đồng</span>
                    </div>
                  </li>
                  <li>
                    <div className="d-flex-column">
                      <span className="fs-16 fw-7 text-color">Nước</span>
                      <span>{AppUtils.number_format(3000)} đồng</span>
                    </div>
                  </li>
                  <li>
                    <div className="d-flex-column">
                      <span className="fs-16 fw-7 text-color">Internet</span>
                      <span>{AppUtils.number_format(3000)} đồng</span>
                    </div>
                  </li>
                </ul>
                <div className="d-flex-column">
                  <span className="fs-16 fw-7 text-color">Địa chỉ</span>
                  <span>{data.wardName}, {data.districtName}, {data.provinceName}</span>
                </div>
              </div>

              <div className="room_detail__content__info_title mt-5">
                <ReconciliationOutlined className="room_detail__content__detail_icon--info" />
                <span className="mr-3">Tiện ích</span>
              </div>
              <div className="room_detail__content__info_detail mt-3">
                <ul className="room_detail__content__info_detail__list">
                  <li>
                    <WifiOutlined className="room_detail__content__detail_icon" />
                    <span>Wifi</span>
                  </li>
                  <li>
                    <WifiOutlined className="room_detail__content__detail_icon" />
                    <span>Toilet</span>
                  </li>
                  <li>
                    <WifiOutlined className="room_detail__content__detail_icon" />
                    <span>Wifi</span>
                  </li>
                  <li>
                    <WifiOutlined className="room_detail__content__detail_icon" />
                    <span>Wifi</span>
                  </li>
                  <li>
                    <WifiOutlined className="room_detail__content__detail_icon" />
                    <span>Wifi</span>
                  </li>
                  <li>
                    <WifiOutlined className="room_detail__content__detail_icon" />
                    <span>Wifi</span>
                  </li>
                </ul>
              </div>
              {data.description ? (
                <div className="room_detail__content__info_title mt-5">
                  <EditOutlined className="room_detail__content__detail_icon--info" />
                  <span className="mr-3">Mô tả thêm</span>
                </div>
              ) : null}
              {data.description ? (
                <div className="mt-3 room_detail__content__description">
                  <div
                    className={classnames({
                      'show': showMore,
                      'short_intro': !showMore,
                      'show_fade': showDescFade
                    })}
                    id="room-des">
                    <span dangerouslySetInnerHTML={{ __html: data.description }} />
                  </div>
                  {showSeeMore && (
                    <span className="see_more main-text cursor-pointer" onClick={() => { this.handleShowMore() }}>
                      {showMore ? "Thu gọn" : "Đọc thêm"}
                    </span>
                  )}
                </div>
              ) : null}
            </div>
            <div className="room_detail__content__right">
              <div className="room_detail__content__right__title">
                <span>Liên hệ thuê phòng</span>
              </div>
              <div className="room_detail__content__right__content">
                <div className="mt-3 mb-2">
                  <UserOutlined className="room_detail__content__detail_icon" />
                  <span className="fs-18">Mr A</span>
                </div>
                <div>
                  <PhoneOutlined className="room_detail__content__detail_icon" />
                  <span className="fs-18">09888888888</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}

RoomDetail.propTypes = {
  history: PropTypes.object,
  login: PropTypes.func,
  processing: PropTypes.bool,
  data: PropTypes.object,
  error: PropTypes.object,
  shows: PropTypes.array,
  noFooter: PropTypes.bool
}
const mapStateToProps = state => {
  return {
    processing: state.houses.processing,
    detailData: state.houses.detailData
  }
}

const mapDispatchToProps = dispatch => ({
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(RoomDetail))
