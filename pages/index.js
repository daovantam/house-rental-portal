import React, { Component } from 'react'
import Layout from '../components/layout'
import { connect } from 'react-redux'
import Router, { withRouter } from 'next/router'
import Head from 'next/head'
import HouseActions from '../redux/_house-redux'
import LocationActions from '../redux/_location-redux'
import CommonActions from '../redux/_common-redux'
import Link from 'next/link'
import i18n from '../i18n';
import Slider from "react-slick"
import { Select, Button, Input } from 'antd'
import { SearchOutlined, CloseOutlined, RightOutlined, EnvironmentOutlined } from '@ant-design/icons';
import { Slick } from '../constants'
import RoomList from '../components/room/room-list'
import { MetaSearch } from '../constants'
import Search from '../components/search/search'

const { Option } = Select;
const priceList = MetaSearch.PRICE_LIST
const areaList = MetaSearch.AREA_LIST
class HomePage extends Component {
  static async getInitialProps({ Component, ctx }) {
    console.log("123", Component, ctx)
  }
  constructor(props) {
    super(props)
    this.state = {
      visible: false,
      classNameSearchSlide: "slideClose mt-0",
      provinceId: "",
      districtId: "",
      wardId: "",
      price: "",
      acreage: "",
      categoryId: "",
      title: null
    }
  }

  componentDidMount() {
    this.props.getListHouseCategory()
    let params = {
      direction: true,
      matching: [
      ],
      orderBy: "updatedTimestamp",
      page: 0,
      pageSize: 12
    }
    this.props.getNewHouse(params)
    let paramsProvince = {
      direction: false,
      matching: [],
      orderBy: "id",
      page: 0,
      pageSize: 100
    }
    this.props.getListProvince(paramsProvince)
  }

  showListHouse = () => {
    Router.push("/room").then(() => window.scrollTo(0, 0))
  }

  toHouseCategory = (name, id) => {
    let params = {
      direction: true,
      matching: [
        {
          key: "categoryId",
          operation: ":",
          orPredicate: true,
          value: id
        }
      ],
      orderBy: "id",
      page: 0,
      pageSize: 12
    }
    this.props.searchHouse(params)
    Router.push(`/search?categoryId=${id}`).then(() => window.scrollTo(0, 0))
  }

  
  render() {
    const listHouse = this.props.topHouse?.list_object
    const { listProvince, listDistrict, listWard, listHouseCategory } = this.props
    return (
      <Layout>
        <Head>
          <title>Trang chủ | HouseRental</title>
        </Head>
        <div className="home_page">
          <div className="home_page__slider">
            <Slider {...Slick.SLIDE_HEADER}>
              <div className="home_page__slider__img">
                <img src={require("../assets/images/image-1.jpg")} />
              </div>
              <div className="home_page__slider__img">
                <img src={require("../assets/images/image-2.jpg")} />
              </div>
              <div className="home_page__slider__img">
                <img src={require("../assets/images/image-3.jpg")} />
              </div>
              <div className="home_page__slider__img">
                <img src={require("../assets/images/image-4.jpg")} />
              </div>
              <div className="home_page__slider__img">
                <img src={require("../assets/images/image-5.jpg")} />
              </div>
            </Slider>
            <div className="home_page__search_container" >
              <h2>TÌM KIẾM ĐỊA CHỈ THUÊ NHÀ UY TÍN, CHẤT LƯỢNG </h2>
              <Search/>
            </div>

          </div>
          <div className="container-content greeting mt-5">
            <h3 className="fw-7 mb-0">Chào mừng đến với House Rental </h3>
            <p className="fs-16">Tìm kiếm phòng trọ, căn hộ dễ dàng, thuận tiện</p>
          </div>
          <div className="container-content category mt-4">
            <Slider {...Slick.SLIDE_CONTENT}>
              {listHouseCategory && listHouseCategory.map((item, index) => {
                return (
                  <div className="category__item mt-2 mb-2 cursor-pointer" key={index} onClick={() => { this.toHouseCategory(item.name, item.id) }}>
                    <img src={require("../assets/images/image-2.jpg")} />
                    <div>
                      <h5 className="category_name">{item.name}</h5>
                    </div>
                  </div>
                )
              })}
            </Slider>
          </div>
          <div className="container-content home_page__top_house mt-5">
            <div className="home_page__top_house__header d-flex mb-3">
              <div className="d-flex-align-center">
                <h3 className="fw-7 mb-0">Phòng mới nhất</h3>
              </div>
              <div className="d-flex-center" onClick={this.showListHouse}>
                <span className="color-main fs-16 cursor-pointer">Xem tất cả <RightOutlined className="fs-12" /></span>
              </div>
            </div>
            <RoomList roomList={listHouse} />
            <div className="d-flex-center-end" onClick={this.showListHouse}>
              <span className="color-main fs-16 cursor-pointer">Xem tất cả <RightOutlined className="fs-12" /></span>
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}
const mapStateToProps = state => {
  return {
    processing: state.houses.processing,
    topHouse: state.houses.listTopHouse,
    listProvince: state.location.listProvince,
    listDistrict: state.location.listDistrict,
    listWard: state.location.listWard,
    listHouseCategory: state.common.listHouseCategories,
    error: state.houses.error
  }
}

const mapDispatchToProps = dispatch => ({
  getNewHouse: data => dispatch(HouseActions.getTopHouseRequest(data)),
  getListHouse: data => dispatch(HouseActions.searchRequest(data)),
  getListProvince: data => dispatch(LocationActions.getProvinceRequest(data)),
  getListDistrict: data => dispatch(LocationActions.getDistrictRequest(data)),
  getListWard: data => dispatch(LocationActions.getWardRequest(data)),
  getListHouseCategory: () => dispatch(CommonActions.getHouseCategoryRequest()),
  searchHouse: data => dispatch(HouseActions.searchRequest(data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(HomePage))

