import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'next/router'
import Layout from '../components/layout'
import Head from 'next/head'
import {
  Tabs
} from 'antd';
import { UnorderedListOutlined, FolderAddOutlined } from '@ant-design/icons'
import Link from 'next/link'
import CreatNewRoom from '../components/host-create-new-room'
import HostListRoom from '../components/host-list-room'
import { ThumbUpSharp } from '@material-ui/icons'
const { TabPane } = Tabs;

class Host extends Component {
  state = {
    keyTab : "1"
  }

  toListRoom = () => {
    window.scrollTo(0, 0)
    this.setState({
      keyTab : "1"
    })
  }

  changeActiveKey = (key) => {
    this.setState({
      keyTab : key
    })
  }

  render() {
    console.log("vao day")
    return (
      <Layout {...this.props}>
        <Head>
          <title>{'Đối tác'}</title>
        </Head>
        <div className="container-content">
          <Tabs className="mt-5"  activeKey={this.state.keyTab} type="card" size="large" onChange={this.changeActiveKey}>
            <TabPane
              tab={
                <span>
                  <UnorderedListOutlined />
                  Danh sách phòng đã đăng
                </span>
              }
              key="1"
            >
              <HostListRoom />
            </TabPane>
            <TabPane
              tab={
                <span>
                  <FolderAddOutlined />
                  Đăng phòng mới
                </span>
              }
              key="2"
            >
              <CreatNewRoom toListRoom={this.toListRoom}/>
            </TabPane>
          </Tabs>

        </div>
      </Layout>
    )
  }
}



const mapStateToProps = state => {
  return {
    processing: state.auths.processing,
    data: state.auths.data,
    error: state.auths.error
  }
}

const mapDispatchToProps = dispatch => ({
  login: data => dispatch(AuthsActions.loginRequest(data))
})


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Host))
