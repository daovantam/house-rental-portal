import React, { Component } from 'react'
import Layout from '../components/layout'
import { connect } from 'react-redux'
import Router, { withRouter } from 'next/router'
import Head from 'next/head'
import HouseActions from '../redux/_house-redux'
import Link from 'next/link'
import i18n from '../i18n';
import PaginationComponent from '../components/pagination'
import RoomList from '../components/room/room-list'
import Spinner from '../components/spinner'
import { MetaSearch } from '../constants'

class SearchHouse extends Component {

  componentDidMount() {
    console.log("vao day")
    let query = Router.query
    if (query) {
      let matching = []
      if (query.title) {
        matching.push({
          key: "title",
          operation: "like",
          orPredicate: false,
          value: query.title
        })
      }
      if (query.provinceId) {
        matching.push({
          key: "provinceId",
          operation: ":",
          orPredicate: false,
          value: query.provinceId
        })
      }
      if (query.districtId) {
        matching.push({
          key: "districtId",
          operation: ":",
          orPredicate: false,
          value: query.districtId
        })
      }
      if (query.wardId) {
        matching.push({
          key: "wardId",
          operation: ":",
          orPredicate: false,
          value: query.wardId
        })
      }
      if (query.categoryId) {
        matching.push({
          key: "categoryId",
          operation: ":",
          orPredicate: false,
          value: query.categoryId
        })
      }
      if (query.price_range) {
        MetaSearch.PRICE_LIST.map((item) => {
          if (Number(query.price_range) === item.id) {
            matching.push(
              {
                key: "price",
                operation: ">=",
                orPredicate: false,
                value: item.from
              },
              {
                key: "price",
                operation: "<",
                orPredicate: false,
                value: item.to
              }
            )
          }
        })
      }
      if (query.acreage_range) {
        MetaSearch.AREA_LIST.map((item) => {
          if (Number(query.acreage_range) === item.id) {
            matching.push(
              {
                key: "acreage",
                operation: ">=",
                orPredicate: false,
                value: item.from
              },
              {
                key: "acreage",
                operation: "<",
                orPredicate: false,
                value: item.to
              }
            )
          }
        })
      }
      let params = {
        direction: true,
        matching: matching,
        orderBy: "updatedTimestamp",
        page: 0,
        pageSize: 12
      }
      this.props.searchHouse(params)
    }
  }

  onSelectPageChange = (page) => {
    let query = Router.query
    if (query) {
      let matching = []
      if (query.title) {
        matching.push({
          key: "title",
          operation: "like",
          orPredicate: false,
          value: query.title
        })
      }
      if (query.provinceId) {
        matching.push({
          key: "provinceId",
          operation: ":",
          orPredicate: false,
          value: query.provinceId
        })
      }
      if (query.districtId) {
        matching.push({
          key: "districtId",
          operation: ":",
          orPredicate: false,
          value: query.districtId
        })
      }
      if (query.wardId) {
        matching.push({
          key: "wardId",
          operation: ":",
          orPredicate: false,
          value: query.wardId
        })
      }
      if (query.categoryId) {
        matching.push({
          key: "categoryId",
          operation: ":",
          orPredicate: false,
          value: query.categoryId
        })
      }
      if (query.price_range) {
        MetaSearch.PRICE_LIST.map((item) => {
          if (Number(query.price_range) === item.id) {
            matching.push(
              {
                key: "price",
                operation: ">=",
                orPredicate: false,
                value: item.from
              },
              {
                key: "price",
                operation: "<",
                orPredicate: false,
                value: item.to
              }
            )
          }
        })
      }
      if (query.acreage_range) {
        MetaSearch.AREA_LIST.map((item) => {
          if (Number(query.acreage_range) === item.id) {
            matching.push(
              {
                key: "acreage",
                operation: ">=",
                orPredicate: false,
                value: item.from
              },
              {
                key: "acreage",
                operation: "<",
                orPredicate: false,
                value: item.to
              }
            )
          }
        })
      }
      let params = {
        direction: true,
        matching: matching,
        orderBy: "updatedTimestamp",
        page: page - 1,
        pageSize: 12
      }
      this.props.getListHouse(params)
    }
  };

  render() {
    const listHouse = this.props.searchData?.list_object
    const total_element = this.props.searchData?.total_element
    const page_size = this.props.searchData?.page_size
    return (
      <Layout>
        <Head>
          <title>Danh sách phòng</title>
        </Head>
        {/* {this.props.processing && <Spinner />} */}
        <div className="container-content" style={{ minHeight: '100vh' }}>
          <div className="mt-5">
            <h3 className="fw-7 mb-0">Danh sách phòng </h3>
            <p className="fs-16">Thông tin cập nhật thường xuyên, chi tiết, chính xác</p>
          </div>
          <RoomList roomList={listHouse} />
          {listHouse && listHouse.length > 0 ? (
            <div>
              <PaginationComponent onChange={this.onSelectPageChange} totalItem={total_element} pageSize={page_size} />
            </div>
          ) : (
              <h4>Không có dữ liệu</h4>
            )}
        </div>
      </Layout>
    )
  }
}
const mapStateToProps = state => {
  return {
    processing: state.houses.processing,
    searchData: state.houses.searchData,
    error: state.houses.error
  }
}

const mapDispatchToProps = dispatch => ({
  searchHouse: data => dispatch(HouseActions.searchRequest(data)),
  getListHouse: data => dispatch(HouseActions.searchRequest(data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(SearchHouse))

