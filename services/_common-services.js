import api from '../api'

export default class CommonService {
  
  getListHouseCategory() {
    return api.get(`/client/api/v1/search/categories`)
  }

}
