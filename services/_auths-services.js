import api from '../api'

export default class AuthsService {

  login(data) {
    return api.post(`/uaa/oauth/token`, data, 'login')
  }

  register(data) {
    return api.post(`/account/create`, data)
  }

  forgotPassword(data) {
    return api.post(`/auths/v1/forgot/`, data)
  }

  socialLogin(data) {
    return api.post(`/social/v1/login/${data.type}`, data)
  }

  userInfo() {
    return api.get(`/uaa/user/current`)
  }

  resetPassword(userName) {
    return api.put(`/account/reset-password/${userName}`)
  }
}
