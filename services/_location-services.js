import api from '../api'

export default class LocationService {
  //client
  getProvince(data) {
    return api.post(`/client/api/v1/search/province`,data)
  }

  getDistrict(data) {
    return api.post(`/client/api/v1/search/district`,data)
  }

  getWard(data) {
    return api.post(`/client/api/v1/search/ward`,data)
  }

  
}
