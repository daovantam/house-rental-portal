import api from '../api'

export default class HouseService {
  //client
  search(data) {
    return api.post(`/client/api/v1/search/house`, data)
  }

  //manager
  getDetailHouse(id) {
    return api.get(`/manage/api/v1/house/${id}`)
  }

  createNewHouse(data) {
    return api.post(`/manage/api/v1/house`, data)
  }
}
