/**
 * @author Nam NH
 * Center point to export instances of services
 */
import AuthsService from './_auths-services'
import HouseService from './_house-services'
import LocationService from './_location-services'
import CommonService from './_common-services'

export const authsService = new AuthsService()
export const houseService = new HouseService()
export const locationService = new LocationService()
export const commonService = new CommonService()
