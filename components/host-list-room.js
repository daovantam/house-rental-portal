import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'next/router'
import Layout from '../components/layout'
import Head from 'next/head'
import { Button, Modal } from 'antd';
import { TagsOutlined, SwitcherOutlined, TeamOutlined, EnvironmentOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons'
import Link from 'next/link'
import PaginationComponent from '../components/pagination'

class HostListRoom extends Component {
  state = {
    visibleDeleteModal: false,
  }

  showModal = () => {
    this.setState({
      visibleDeleteModal: true,
    });
  };

  handleOk = e => {
    this.setState({
      visibleDeleteModal: false,
    });
  };

  handleCancel = e => {
    this.setState({
      visibleDeleteModal: false,
    });
  };

  render() {
    return (
      <div className="host_list_room">
        <div className="detail_item d-flex">
          <div className="room_image">
            <img src={require("../assets/images/image-1.jpg")} />
          </div>
          <div className="room_content">
            <div className="room_name">
              <h5 >Phòng cho thuê phường Mễ Trì, quận Nam Từ Liêm </h5>
            </div>
            <div className="room_type"><TagsOutlined className="fs-18" /> Phòng cho thuê</div>
            <div className="fs-16">
              <span className="mr-4"><SwitcherOutlined /> 180m²</span>
              <span className="mr-4"><TeamOutlined /> 2 phòng ngủ</span>
              <span className="fs-20 fw-7 color-price">2.000.000 đ</span>
            </div>
            <div>
              <span className="mr-2"><EnvironmentOutlined style={{ fontSize: '18px' }} /></span>
              <span>Quận Nam Từ Liêm, Hà Nội</span>
            </div>
            <div className="mt-2">
              <Button className="btn-main-outline mr-2" size="small" icon={<EditOutlined />}>
                Chỉnh sửa
              </Button>
              <Button size="small" icon={<DeleteOutlined />} danger onClick={this.showModal}>Xoá</Button>
            </div>
          </div>
        </div>
        <div className="detail_item d-flex">
          <div className="room_image">
            <img src={require("../assets/images/image-3.jpg")} />
          </div>
          <div className="room_content">
              <div className="room_name">
              <h5 >Phòng cho thuê phường Mễ Trì, quận Nam Từ Liêm </h5>
            </div>
            <div className="room_type"><TagsOutlined className="fs-18" /> Phòng cho thuê</div>
            <div className="fs-16">
              <span className="mr-4"><SwitcherOutlined /> 180m²</span>
              <span className="mr-4"><TeamOutlined /> 2 phòng ngủ</span>
              <span className="fs-20 fw-7 color-price">2.000.000 đ</span>
            </div>
            <div>
              <span className="mr-2"><EnvironmentOutlined style={{ fontSize: '18px' }} /></span>
              <span>Quận Nam Từ Liêm, Hà Nội</span>
            </div>
            <div className="mt-2">
              <Button className="btn-main-outline mr-2" size="small" icon={<EditOutlined />}>
                Chỉnh sửa
                  </Button>
              <Button size="small" icon={<DeleteOutlined />} onClick={this.showModal} danger>Xoá</Button>
            </div>
          </div>
        </div>
        <div className="detail_item d-flex">
          <div className="room_image">
            <img src={require("../assets/images/image-5.jpg")} />
          </div>
          <div className="room_content">
              <div className="room_name">
              <h5 >Phòng cho thuê phường Mễ Trì, quận Nam Từ Liêm </h5>
            </div>
            <div className="room_type"><TagsOutlined className="fs-18" /> Phòng cho thuê</div>
            <div className="fs-16">
              <span className="mr-4"><SwitcherOutlined /> 180m²</span>
              <span className="mr-4"><TeamOutlined /> 2 phòng ngủ</span>
              <span className="fs-20 fw-7 color-price">2.000.000 đ</span>
            </div>
            <div>
              <span className="mr-2"><EnvironmentOutlined style={{ fontSize: '18px' }} /></span>
              <span>Quận Nam Từ Liêm, Hà Nội</span>
            </div>
            <div className="mt-2">
              <Button className="btn-main-outline mr-2" size="small" icon={<EditOutlined />}>
                Chỉnh sửa
                  </Button>
              <Button size="small" icon={<DeleteOutlined />} onClick={this.showModal} danger>Xoá</Button>
            </div>
          </div>
        </div>
        <div className="detail_item d-flex">
          <div className="room_image">
            <img src={require("../assets/images/image-4.jpg")} />
          </div>
          <div className="room_content">
              <div className="room_name">
              <h5 >Phòng cho thuê phường Mễ Trì, quận Nam Từ Liêm </h5>
            </div>
            <div className="room_type"><TagsOutlined className="fs-18" /> Phòng cho thuê</div>
            <div className="fs-16">
              <span className="mr-4"><SwitcherOutlined /> 180m²</span>
              <span className="mr-4"><TeamOutlined /> 2 phòng ngủ</span>
              <span className="fs-20 fw-7 color-price">2.000.000 đ</span>
            </div>
            <div>
              <span className="mr-2"><EnvironmentOutlined style={{ fontSize: '18px' }} /></span>
              <span>Quận Nam Từ Liêm, Hà Nội</span>
            </div>
            <div className="mt-2">
              <Button className="btn-main-outline mr-2" size="small" icon={<EditOutlined />}>
                Chỉnh sửa
                  </Button>
              <Button size="small" icon={<DeleteOutlined />} onClick={this.showModal} danger>Xoá</Button>
            </div>
          </div>
        </div>
        <div className="detail_item d-flex">
          <div className="room_image">
            <img src={require("../assets/images/image-5.jpg")} />
          </div>
          <div className="room_content">
              <div className="room_name">
              <h5 >Phòng cho thuê phường Mễ Trì, quận Nam Từ Liêm </h5>
            </div>
            <div className="room_type"><TagsOutlined className="fs-18" /> Phòng cho thuê</div>
            <div className="fs-16">
              <span className="mr-4"><SwitcherOutlined /> 180m²</span>
              <span className="mr-4"><TeamOutlined /> 2 phòng ngủ</span>
              <span className="fs-20 fw-7 color-price">2.000.000 đ</span>
            </div>
            <div>
              <span className="mr-2"><EnvironmentOutlined style={{ fontSize: '18px' }} /></span>
              <span>Quận Nam Từ Liêm, Hà Nội</span>
            </div>
            <div className="mt-2">
              <Button className="btn-main-outline mr-2" size="small" icon={<EditOutlined />}>
                Chỉnh sửa
                  </Button>
              <Button size="small" icon={<DeleteOutlined />} onClick={this.showModal} danger>Xoá</Button>
            </div>
          </div>
        </div>
        <Modal
          visible={this.state.visibleDeleteModal}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <p>Bạn có thực sự muốn xoá phòng này?</p>
        </Modal>
        <div className="mt-3">
          <PaginationComponent totalItem={50} pageSize={20} />
        </div>
      </div>
    )
  }
}



const mapStateToProps = state => {
  return {
    processing: state.auths.processing,
    data: state.auths.data,
    error: state.auths.error
  }
}

const mapDispatchToProps = dispatch => ({
  login: data => dispatch(AuthsActions.loginRequest(data))
})


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(HostListRoom))
