import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'next/router'
import {
  Form,
  Input,
  Select,
  Row,
  Col,
  Checkbox,
  Button,
  InputNumber,
  Upload,
  Modal,
  message,
  Descriptions
} from 'antd';
import { PlusOutlined } from '@ant-design/icons'
import Link from 'next/link'
import LocationActions from '../redux/_location-redux'
import HouseActions from '../redux/_house-redux'
import CommonActions from '../redux/_common-redux'
import CKEditor from 'ckeditor4-react'
// import CKEditor from '@ckeditor/ckeditor5-react';
// import ClassicEditor from '@ckeditor/ckeditor5-build-classic';


const { Option } = Select;
const { TextArea } = Input;

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 4,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 18,
    },
  },
};

const detailInforLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
    md: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};


function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

function imageCoverGetBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}


function beforeUpload(file) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJpgOrPng && isLt2M;
}
class CreateNewRoom extends Component {
  formRef = React.createRef();

  state = {
    previewVisible: false,
    previewImage: '',
    previewTitle: '',
    fileList: [],
    description: ''
  }
  componentDidMount() {
    this.props.getListHouseCategory()
    let paramsProvince = {
      direction: false,
      matching: [],
      orderBy: "id",
      page: 0,
      pageSize: 100
    }
    this.props.getListProvince(paramsProvince)
  }

  handleCancel = () => this.setState({ previewVisible: false });

  handlePreview = async file => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }

    this.setState({
      previewImage: file.url || file.preview,
      previewVisible: true,
      previewTitle: file.name || file.url.substring(file.url.lastIndexOf('/') + 1),
    });
  };

  handleChange = ({ fileList }) => this.setState({ fileList });

  handleChangeImageCover = info => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      imageCoverGetBase64(info.file.originFileObj, imageUrl =>
        this.setState({
          imageUrl,
          loading: false,
        }),
      );
    }
  };

  onEditorChange = (evt) => {
    this.setState({
      description: evt.editor.getData(),
    });
  };

  resetForm = () => {
    this.formRef.current.resetFields();
    this.setState({
      fileList: []
    })
  }

  createHouse = (values) => {
    let formData = new FormData()
    formData.append('acreage', values.acreage)
    formData.append('categoryId', values.categoryId)
    formData.append('images', values.imageList.fileList),
    formData.append('numberOfBedroom', values.numberOfBedRoom)
    formData.append('price', values.price)
    formData.append('title', values.title)
    formData.append('wardId', values.wardId)
    formData.append('description', this.state.description)
    this.props.createHouse(formData)
    this.props.toListRoom()
    this.resetForm()
  }

  handleSelectProvince = (data) => {
    let paramsDistrict = {
      direction: false,
      matching: [
        {
          key: "provinceId",
          operation: ":",
          orPredicate: true,
          value: data
        }
      ],
      orderBy: "id",
      page: 0,
      pageSize: 100
    }
    this.props.getListDistrict(paramsDistrict)
  }

  handleSelectDistrict = (data) => {
    let paramsWard = {
      direction: false,
      matching: [
        {
          key: "districtId",
          operation: ":",
          orPredicate: true,
          value: data
        }
      ],
      orderBy: "id",
      page: 0,
      pageSize: 100
    }
    this.props.getListWard(paramsWard)
  }

  render() {

    const { previewVisible, previewImage, fileList, previewTitle, loading, imageUrl } = this.state;
    const { listProvince, listDistrict, listWard, listHouseCategory } = this.props
    const uploadButton = (
      <div>
        <PlusOutlined />
        <div style={{ marginTop: 8 }}>Upload</div>
      </div>
    );

    return (
      <div className="">
        <Form
          {...formItemLayout}
          name="create"
          ref={this.formRef}
          initialValues={{
          }}
          scrollToFirstError
          onFinish={this.createHouse}
        >
          <div className="d-flex-center-end mt-3 mb-3">
            <Button type="primary" htmlType="submit" size="large">
              <span className="fs-16 fw-7">Đăng bài</span>
            </Button>
          </div>
          <div className="host_creat_room">
            <div className="title">
              <h4 className="text-color fw-7 ">Thông tin cơ bản</h4>
            </div>
            <div className="pl-2 pr-2">
              <Form.Item
                name="title"
                label="Tiêu đề"
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng nhập tiêu đề bài đăng!',
                  },
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                label="Loại phòng"
                name="categoryId"
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng lựa chọn loại phòng',
                  },
                ]}>
                <Select
                  showSearch
                  style={{ width: 180 }}
                  placeholder="Lựa chọn loại phòng"
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {listHouseCategory && listHouseCategory.map((item, index) => {
                    return (
                      <Option value={item.id} key={index}>{item.name}</Option>
                    )
                  })}
                </Select>
              </Form.Item>
              <Form.Item
                label="Giá tiền"
                name="price"
                rules={[
                  {
                    required: true,
                    message: ' ',
                  },
                ]}
              >
                <div className="d-flex">
                  <Form.Item
                    name="price"
                    rules={[
                      {
                        required: true,
                        message: 'Vui lòng nhập giá tiền',
                      },
                    ]}>
                    <InputNumber
                      style={{ width: 180 }}
                      min={1}
                      formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                      parser={value => value.replace(/\$\s?|(,*)/g, '')}
                    />
                  </Form.Item>
                  <span className="ml-2 mr-2 mt-2 fs-16">/</span>
                  <Form.Item
                    name="pricePerUnit"
                    rules={[
                      {
                        required: true,
                        message: 'Vui lòng nhập đơn vị',
                      },
                    ]}>
                    <Select
                      name="unit"
                      showSearch
                      style={{ width: 100 }}
                      optionFilterProp="children"
                      filterOption={(input, option) =>
                        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                      }
                    >
                      <Option value="room">Phòng</Option>
                      <Option value="apartment">Căn</Option>
                      <Option value="people">Người</Option>
                    </Select>
                  </Form.Item>
                </div>
              </Form.Item>
              <Form.Item label="Thông tin cơ bản">
                <div className="row mt-5 mr-0 ml-0">
                  <div className="w-100">
                    <Descriptions
                      bordered
                      column={{ xxl: 3, xl: 3, lg: 2, md: 2, sm: 1, xs: 1 }}
                    >
                      <Descriptions.Item label="Diện tích">
                        <Form.Item name="acreage"
                          noStyle
                          rules={[
                            {
                              required: true,
                              message: 'Vui lòng nhập diện tích',
                            },
                          ]}>
                          <InputNumber
                            formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                            parser={value => value.replace(/\$\s?|(,*)/g, '')}
                            min={1}
                          />
                        </Form.Item>

                        <span> m2</span>
                      </Descriptions.Item>
                      <Descriptions.Item label="Số phòng ngủ">
                        <Form.Item name="numberOfBedRoom" noStyle>
                          <InputNumber min={1} />
                        </Form.Item>
                      </Descriptions.Item>
                      <Descriptions.Item label="Tiền điện">
                        <Form.Item name="electricMoney"
                          noStyle>
                          <InputNumber
                            style={{ width: 100 }}
                            min={1}
                            formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                            parser={value => value.replace(/\$\s?|(,*)/g, '')}
                          />
                        </Form.Item>
                        <span> / số</span>
                      </Descriptions.Item>
                      <Descriptions.Item label="Tiền nước">
                        <Form.Item name="waterMoney"
                          noStyle>
                          <InputNumber
                            className="w-100"
                            min={1}
                            formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                            parser={value => value.replace(/\$\s?|(,*)/g, '')}
                          />
                        </Form.Item>
                      </Descriptions.Item>
                      <Descriptions.Item label="Internet">
                        <Form.Item name="internetMoney"
                          noStyle>
                          <InputNumber
                            className="w-100"
                            min={1}
                            formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                            parser={value => value.replace(/\$\s?|(,*)/g, '')}
                          />
                        </Form.Item>
                      </Descriptions.Item>
                      <Descriptions.Item label="Đặt cọc">
                        <Form.Item name="deposit"
                          noStyle>
                          <InputNumber
                            style={{ width: 100 }}
                            min={1}
                            formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                            parser={value => value.replace(/\$\s?|(,*)/g, '')}
                          />
                        </Form.Item>
                      </Descriptions.Item>
                    </Descriptions>
                  </div>
                  <div>
                    {/* <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                    <Form.Item
                      label="Diện tích"
                      {...detailInforLayout}
                    >
                      <Form.Item name="acreage"
                        noStyle
                        rules={[
                          {
                            required: true,
                            message: 'Vui lòng nhập diện tích',
                          },
                        ]}>
                        <InputNumber
                          style={{ width: 100 }}
                          formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                          parser={value => value.replace(/\$\s?|(,*)/g, '')}
                        />
                      </Form.Item>
                      <span> m2</span>
                    </Form.Item>
                  </div>
                  <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                    <Form.Item
                      label="Sức chứa"
                      {...detailInforLayout}
                    >
                      <Form.Item name="numberOfPeople" noStyle>
                        <InputNumber />
                      </Form.Item>
                      <span> người</span>
                    </Form.Item>
                  </div>
                  <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                    <Form.Item
                      label="Số phòng ngủ"
                      {...detailInforLayout}
                    >
                      <Form.Item name="numberOfBedRoom" noStyle>
                        <InputNumber />
                      </Form.Item>
                      <span> người</span>
                    </Form.Item>
                  </div>
                  <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                    <Form.Item
                      label="Tiền điện"
                      {...detailInforLayout}
                    >
                      <Form.Item name="electricMoney"
                        noStyle>
                        <InputNumber
                          style={{ width: 100 }}
                          formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                          parser={value => value.replace(/\$\s?|(,*)/g, '')}
                        />
                      </Form.Item>
                      <span> / số</span>
                    </Form.Item>
                  </div>
                  <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                    <Form.Item
                      label="Tiền nước"
                      {...detailInforLayout}
                    >
                      <Form.Item name="waterMoney"
                        noStyle>
                        <InputNumber
                          style={{ width: 100 }}
                          formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                          parser={value => value.replace(/\$\s?|(,*)/g, '')}
                        />
                      </Form.Item>
                    </Form.Item>
                  </div>
                  <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                    <Form.Item
                      label="Internet"
                      {...detailInforLayout}
                    >
                      <Form.Item name="internetMoney"
                        noStyle>
                        <InputNumber
                          style={{ width: 100 }}
                          formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                          parser={value => value.replace(/\$\s?|(,*)/g, '')}
                        />
                      </Form.Item>
                    </Form.Item>
                  </div>
                  <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                    <Form.Item
                      label="Đặt cọc"
                      {...detailInforLayout}
                    >
                      <Form.Item name="deposit"
                        noStyle>
                        <InputNumber
                          style={{ width: 100 }}
                          formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                          parser={value => value.replace(/\$\s?|(,*)/g, '')}
                        />
                      </Form.Item>
                    </Form.Item>
                  </div>
                 */}
                  </div>
                </div>
              </Form.Item>
              <Form.Item name="amenities" label="Tiện ích">
                <Checkbox.Group className="w-100">
                  <Row>
                    <Col span={8}>
                      <Checkbox
                        value="wifi"
                        style={{
                          lineHeight: '32px',
                        }}
                      >
                        Wifi
                  </Checkbox>
                    </Col>
                    <Col span={8}>
                      <Checkbox
                        value="air-con"
                        style={{
                          lineHeight: '32px',
                        }}
                      >
                        Điều hoà
                  </Checkbox>
                    </Col>
                    <Col span={8}>
                      <Checkbox
                        value="wc"
                        style={{
                          lineHeight: '32px',
                        }}
                      >
                        WC riêng
                </Checkbox>
                    </Col>
                    <Col span={8}>
                      <Checkbox
                        value="kitchen"
                        style={{
                          lineHeight: '32px',
                        }}
                      >
                        Bếp
                </Checkbox>
                    </Col>
                    <Col span={8}>
                      <Checkbox
                        value="refri"
                        style={{
                          lineHeight: '32px',
                        }}
                      >
                        Tủ lạnh
                </Checkbox>
                    </Col>
                    <Col span={8}>
                      <Checkbox
                        value="private"
                        style={{
                          lineHeight: '32px',
                        }}
                      >
                        Không chung chủ
                </Checkbox>
                    </Col>
                    <Col span={8}>
                      <Checkbox
                        value="park"
                        style={{
                          lineHeight: '32px',
                        }}
                      >
                        Nhà để xe
                </Checkbox>
                    </Col>
                    <Col span={8}>
                      <Checkbox
                        value="tv"
                        style={{
                          lineHeight: '32px',
                        }}
                      >
                        TV
                </Checkbox>
                    </Col>
                  </Row>
                </Checkbox.Group>
              </Form.Item>
              <Form.Item
                name="description"
                label="Mô tả"
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng nhập mô tả bài đăng!',
                  },
                ]}
              >
                {/* <TextArea rows={5} /> */}
                <CKEditor data="" onChange={this.onEditorChange} />
              </Form.Item>
              <Form.Item
                label="Ảnh chi tiết"
                name="imageList"
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng đăng ảnh chi tiết',
                  },
                ]}
              >
                <Upload
                  action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                  listType="picture-card"
                  fileList={fileList}
                  onPreview={this.handlePreview}
                  onChange={this.handleChange}
                >
                  {fileList.length >= 8 ? null : uploadButton}
                </Upload>
              </Form.Item>
              <Modal
                visible={previewVisible}
                title={previewTitle}
                footer={null}
                onCancel={this.handleCancel}
              >
                <img alt="example" style={{ width: '100%' }} src={previewImage} />
              </Modal>
              <Form.Item
                label="Địa chỉ"
              >
                <div className="row">
                  <div className="col-12 col-sm-12 col-md-6 col-lg-4">
                    <Form.Item name="provinceId">
                      <Select
                        showSearch
                        style={{ width: 180 }}
                        placeholder="Tỉnh/thành phố"
                        onSelect={this.handleSelectProvince}
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                          option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {listProvince && listProvince.map((item, index) => {
                          return (
                            <Option value={item.id} key={index}>{item.name}</Option>
                          )
                        })}
                      </Select>
                    </Form.Item>
                  </div>
                  <div className="col-12 col-sm-12 col-md-6 col-lg-4">
                    <Form.Item name="districtId">
                      <Select
                        showSearch
                        style={{ width: 180 }}
                        placeholder="Quận/huyện"
                        optionFilterProp="children"
                        onSelect={this.handleSelectDistrict}
                        filterOption={(input, option) =>
                          option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {listDistrict && listDistrict.map((item, index) => {
                          return (
                            <Option value={item.id} key={index}>{item.name}</Option>
                          )
                        })}
                      </Select>
                    </Form.Item></div>
                  <div className="col-12 col-sm-12 col-md-6 col-lg-4">
                    <Form.Item name="wardId">
                      <Select
                        showSearch
                        style={{ width: 180 }}
                        placeholder="Phường/xã"
                        optionFilterProp="children"
                        filterOption={(input, option) =>
                          option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {listWard && listWard.map((item, index) => {
                          return (
                            <Option value={item.id} key={index}>{item.name}</Option>
                          )
                        })}
                      </Select>
                    </Form.Item>
                  </div>
                </div>
              </Form.Item>

            </div>
          </div>
          {/* <div className="host_creat_room mt-5">
            <div className="title">
              <h4 className="text-color fw-7 ">Thông tin liên hệ</h4>
            </div>
            <div className="pl-2 pr-2">
              <Form.Item
                name="contact_name"
                label="Tên liên hệ"
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng nhập tên liên hệ!',
                  },
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                name="contact_address"
                label="Địa chỉ"
              >
                <Input />
              </Form.Item>
              <Form.Item
                name="contact_phone"
                label="Điện thoại"
              >
                <Input style={{ width: 200 }} />
              </Form.Item>
              <Form.Item
                name="contact_email"
                label="Email"
              >
                <Input />
              </Form.Item>

            </div>
          </div>
           */}
          <div className="d-flex-center-end mt-3 mb-3">
            <Button type="primary" htmlType="submit" size="large">
              <span className="fs-16 fw-7">Đăng bài</span>
            </Button>
          </div>
        </Form>

      </div>
    )
  }
}



const mapStateToProps = state => {
  return {
    processing: state.location.processing,
    listProvince: state.location.listProvince,
    listDistrict: state.location.listDistrict,
    listWard: state.location.listWard,
    listHouseCategory: state.common.listHouseCategories,
    error: state.location.error
  }
}

const mapDispatchToProps = dispatch => ({
  getListProvince: data => dispatch(LocationActions.getProvinceRequest(data)),
  getListDistrict: data => dispatch(LocationActions.getDistrictRequest(data)),
  getListWard: data => dispatch(LocationActions.getWardRequest(data)),
  getListHouseCategory: () => dispatch(CommonActions.getHouseCategoryRequest()),
  createHouse: data => dispatch(HouseActions.createHouseRequest(data))
})


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(CreateNewRoom))
