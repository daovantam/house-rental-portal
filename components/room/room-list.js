import React, { Component } from 'react'
import Link from 'next/link'
import { connect } from 'react-redux'
import Router,{ withRouter } from 'next/router'
import HouseActions from '../../redux/_house-redux'
import { TagsOutlined, SwitcherOutlined, TeamOutlined, EnvironmentOutlined } from '@ant-design/icons';
import AppUtils from '../../utils/AppUtils'

class RoomList extends Component {

  getDetailHouse = (id) => {
    // this.props.getDetailHouse(id)
    Router.push(`/room/${id}`).then(() => window.scrollTo(0, 0))
  }
  render() {
    const { roomList } = this.props
    return (
      <div className="room_list">
        <div className="row">
          {roomList ? (
            roomList.map((item, index) => {
              return (
                <div className="col-12 col-sm-12 col-md-4 col-lg-3 mb-4" key={index}>
                  <div className="room_list__card_image">
                    <div className="card-image cursor-pointer" onClick={() => {this.getDetailHouse(item.id)}}>
                      <img src={require("../../assets/images/image-1.jpg")} />
                    </div>
                    <div className="room_list__card_content mt-3">
                      <div className="room_list__card_content__room_type"><TagsOutlined className="fs-18" /> {item.categoryName}</div>
                        <div className="room_list__card_content__room_name mt-2" onClick={() => {this.getDetailHouse(item.id)}}>
                          <h5>{item.title} </h5>
                        </div>
                      <div className="fs-16">
                        <span className="mr-4"><SwitcherOutlined /> {item.acreage}m²</span>
                        <span><TeamOutlined /> {item.numberOfBedroom} phòng ngủ</span>
                      </div>
                      <div className="fs-20 fw-7 color-price">
                        <span>{AppUtils.number_format(item.price)} đ</span>
                      </div>
                      <div>
                        <span className="mr-2"><EnvironmentOutlined style={{ fontSize: '18px' }} /></span>
                        <span>{item.wardName}, {item.districtName}, {item.provinceName}</span>
                      </div>
                    </div>
                  </div>
                </div>
              )
            })
          ) : null}

          {/* <div className="col-12 col-sm-12 col-md-4 col-lg-3 mb-4">
            <div className="room_list__card_image">
              <div className="card_image cursor-pointer">
                <img src={require("../../assets/images/image-3.jpg")} />
              </div>
              <div className="room_list__card_content mt-3">
                <div className="room_list__card_content__room_type"><TagsOutlined style={{ fontSize: '18px' }} /> Phòng cho thuê</div>
                <Link href='/room/1'>
                  <div className="room_list__card_content__room_name mt-2">
                    <h5>Phòng cho thuê phường Mễ Trì, quận Nam Từ Liêm </h5>
                  </div>
                </Link>
                <div className="fs-16">
                  <span className="mr-4"><SwitcherOutlined /> 180m²</span>
                  <span><TeamOutlined /> 2 phòng ngủ</span>
                </div>
                <div className="fs-20 fw-7 color-price">
                  <span>2.000.000 đ</span>
                </div>
                <div>
                  <span className="mr-2"><EnvironmentOutlined style={{ fontSize: '18px' }} /></span>
                  <span>Quận Nam Từ Liêm, Hà Nội</span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-sm-12 col-md-4 col-lg-3 mb-4">
            <div className="room_list__card_image">
              <div className="card-image cursor-pointer">
                <img src={require("../../assets/images/image-4.jpg")} />
              </div>
              <div className="room_list__card_content mt-3">
                <div className="room_list__card_content__room_type"><TagsOutlined style={{ fontSize: '18px' }} /> Phòng cho thuê</div>
                <Link href='/room/1'>
                  <div className="room_list__card_content__room_name mt-2">
                    <h5>Phòng cho thuê phường Mễ Trì, quận Nam Từ Liêm </h5>
                  </div>
                </Link>
                <div className="fs-16">
                  <span className="mr-4"><SwitcherOutlined /> 180m²</span>
                  <span><TeamOutlined /> 2 phòng ngủ</span>
                </div>
                <div className="fs-20 fw-7 color-price">
                  <span>2.000.000 đ</span>
                </div>
                <div>
                  <span className="mr-2"><EnvironmentOutlined style={{ fontSize: '18px' }} /></span>
                  <span>Quận Nam Từ Liêm, Hà Nội</span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-sm-12 col-md-4 col-lg-3 mb-4">
            <div className="room_list__card_image">
              <div className="card-image cursor-pointer">
                <img src={require("../../assets/images/image-5.jpg")} />
              </div>
              <div className="room_list__card_content mt-3">
                <div className="room_list__card_content__room_type"><TagsOutlined style={{ fontSize: '18px' }} /> Phòng cho thuê</div>
                <Link href='/room/1'>
                  <div className="room_list__card_content__room_name mt-2">
                    <h5>Phòng cho thuê phường Mễ Trì, quận Nam Từ Liêm </h5>
                  </div>
                </Link>
                <div className="fs-16">
                  <span className="mr-4"><SwitcherOutlined /> 180m²</span>
                  <span><TeamOutlined /> 2 phòng ngủ</span>
                </div>
                <div className="fs-20 fw-7 color-price">
                  <span>2.000.000 đ</span>
                </div>
                <div>
                  <span className="mr-2"><EnvironmentOutlined style={{ fontSize: '18px' }} /></span>
                  <span>Quận Nam Từ Liêm, Hà Nội</span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-sm-12 col-md-4 col-lg-3 mb-4">
            <div className="room_list__card_image">
              <div className="card-image cursor-pointer">
                <img src={require("../../assets/images/image-1.jpg")} />
              </div>
              <div className="room_list__card_content mt-3">
                <div className="room_list__card_content__room_type"><TagsOutlined style={{ fontSize: '18px' }} /> Phòng cho thuê</div>
                <Link href='/room/1'>
                  <div className="room_list__card_content__room_name mt-2">
                    <h5>Phòng cho thuê phường Mễ Trì, quận Nam Từ Liêm </h5>
                  </div>
                </Link>
                <div className="fs-16">
                  <span className="mr-4"><SwitcherOutlined /> 180m²</span>
                  <span><TeamOutlined /> 2 phòng ngủ</span>
                </div>
                <div className="fs-20 fw-7 color-price">
                  <span>2.000.000 đ</span>
                </div>
                <div>
                  <span className="mr-2"><EnvironmentOutlined style={{ fontSize: '18px' }} /></span>
                  <span>Quận Nam Từ Liêm, Hà Nội</span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-sm-12 col-md-4 col-lg-3 mb-4">
            <div className="room_list__card_image">
              <div className="card-image cursor-pointer">
                <img src={require("../../assets/images/image-3.jpg")} />
              </div>
              <div className="room_list__card_content mt-3">
                <div className="room_list__card_content__room_type"><TagsOutlined style={{ fontSize: '18px' }} /> Phòng cho thuê</div>
                <Link href='/room/1'>
                  <div className="room_list__card_content__room_name mt-2">
                    <h5>Phòng cho thuê phường Mễ Trì, quận Nam Từ Liêm </h5>
                  </div>
                </Link>
                <div className="fs-16">
                  <span className="mr-4"><SwitcherOutlined /> 180m²</span>
                  <span><TeamOutlined /> 2 phòng ngủ</span>
                </div>
                <div className="fs-20 fw-7 color-price">
                  <span>2.000.000 đ</span>
                </div>
                <div>
                  <span className="mr-2"><EnvironmentOutlined style={{ fontSize: '18px' }} /></span>
                  <span>Quận Nam Từ Liêm, Hà Nội</span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-sm-12 col-md-4 col-lg-3 mb-4">
            <div className="room_list__card_image">
              <div className="card-image cursor-pointer">
                <img src={require("../../assets/images/image-4.jpg")} />
              </div>
              <div className="room_list__card_content mt-3">
                <div className="room_list__card_content__room_type"><TagsOutlined style={{ fontSize: '18px' }} /> Phòng cho thuê</div>
                <Link href='/room/1'>
                  <div className="room_list__card_content__room_name mt-2">
                    <h5>Phòng cho thuê phường Mễ Trì, quận Nam Từ Liêm </h5>
                  </div>
                </Link>
                <div className="fs-16">
                  <span className="mr-4"><SwitcherOutlined /> 180m²</span>
                  <span><TeamOutlined /> 2 phòng ngủ</span>
                </div>
                <div className="fs-20 fw-7 color-price">
                  <span>2.000.000 đ</span>
                </div>
                <div>
                  <span className="mr-2"><EnvironmentOutlined style={{ fontSize: '18px' }} /></span>
                  <span>Quận Nam Từ Liêm, Hà Nội</span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-sm-12 col-md-4 col-lg-3 mb-4">
            <div className="room_list__card_image">
              <div className="card-image cursor-pointer">
                <img src={require("../../assets/images/image-5.jpg")} />
              </div>
              <div className="room_list__card_content mt-3">
                <div className="room_list__card_content__room_type"><TagsOutlined style={{ fontSize: '18px' }} /> Phòng cho thuê</div>
                <Link href='/room/1'>
                  <div className="room_list__card_content__room_name mt-2">
                    <h5>Phòng cho thuê phường Mễ Trì, quận Nam Từ Liêm </h5>
                  </div>
                </Link>
                <div className="fs-16">
                  <span className="mr-4"><SwitcherOutlined /> 180m²</span>
                  <span><TeamOutlined /> 2 phòng ngủ</span>
                </div>
                <div className="fs-20 fw-7 color-price">
                  <span>2.000.000 đ</span>
                </div>
                <div>
                  <span className="mr-2"><EnvironmentOutlined style={{ fontSize: '18px' }} /></span>
                  <span>Quận Nam Từ Liêm, Hà Nội</span>
                </div>
              </div>
            </div>
          </div> */}

        </div>

      </div>
    )
  }
}


const mapStateToProps = state => {
  return {
    processing: state.auths.processing,
    data: state.auths.data,
    error: state.auths.error
  }
}

const mapDispatchToProps = dispatch => ({
  getDetailHouse: data => dispatch(HouseActions.getDetailHouseRequest(data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(RoomList))
