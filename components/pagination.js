import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Pagination } from 'antd'

class PaginationComponent extends Component {
  state = {
    pageSize: 10,
    windowWidth: 0
  }
  setWidth = () => {
    this.setState({
      windowWidth: window.innerWidth
    })
  }
  onShowSizeChange = (current, pageSize) => {
    this.setState({
      pageSize: pageSize
    })
  }
  componentDidMount() {
    this.setState({
      pageSize: this.props.pageSize,
      windowWidth: window.innerWidth
    })
    window.addEventListener('resize', this.setWidth)
  }
  render() {
    const { totalItem } = this.props
    const { windowWidth , pageSize} = this.state
    return (
      <div className="d-flex-center mt-2 mb-2">
        <Pagination
          onShowSizeChange={this.onShowSizeChange}
          onChange= {this.props.onChange}
          size={windowWidth > 767 ? "default" : "small"}
          defaultCurrent={1} total={totalItem} pageSize={pageSize}
          locale={{ items_per_page: '/ Trang' }} 
        />
      </div>
    )
  }
}

PaginationComponent.propTypes = {
  totalItem: PropTypes.number,
  pageSize: PropTypes.number,
  onChange: PropTypes.func
}

export default PaginationComponent
