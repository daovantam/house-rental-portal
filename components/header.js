import React, { Component } from 'react'
import { connect } from 'react-redux'
import Router, { withRouter } from 'next/router'
import Link from 'next/link'
import { Drawer, Button, Radio, Space, Select, Input, Menu, Dropdown } from 'antd';
import { MenuUnfoldOutlined, CloseOutlined, SearchOutlined, EnvironmentOutlined, CaretDownFilled, PoweroffOutlined } from '@ant-design/icons';
import classnames from 'classnames'
import AuthsActions from '../redux/_auths-redux'
import { Cookies } from 'react-cookie';
import SearchMobile from '../components/search/search-mobile'

const cookies = new Cookies();
const { Option } = Select;
class HeaderComp extends Component {
  constructor(props) {
    super(props)
    this.state = {
      menu: false,
      anchorEl: null,
      open: false,
      visibleMenu: false,
      placementMenu: 'left',
      visibleSearch: false,
      placementSearch: 'top'
    }
  }
  toggleMenu = () => {
    let mobileMenu = document.getElementById("js-menu");
    mobileMenu.classList.toggle("active");

    let mask = document.getElementById("mask");
    mask.classList.toggle("mask");

    let mainNav = document.getElementById("open-menu");
    mainNav.classList.toggle("opened");
  }

  showDrawerMenu = () => {
    this.setState({
      visibleMenu: true,
    });
  };

  onCloseMenu = () => {
    this.setState({
      visibleMenu: false,
    });
  };

  showDrawerSearch = () => {
    this.setState({
      visibleSearch: true,
    });
  };

  onCloseSearch = () => {
    this.setState({
      visibleSearch: false,
    });
  };

  handleLogout = () => {
    window.localStorage.clear()
    cookies.remove('jwt_auth_token')
    cookies.remove('user_name')
    // Router.push('/')
    window.location.href = "/";
  }

  componentDidMount() {
    if (cookies.get('jwt_auth_token')) {
      this.props.getUserInfor()
    }
  }

  render() {
    const { placementMenu, visibleMenu, placementSearch, visibleSearch } = this.state;
    const data = this.props.userInfor
    let token;
    if (typeof window !== "undefined") {
      token = cookies.get('jwt_auth_token')
    }
    const menuDropdown = (
      <Menu>
        <Menu.Item key="1">
          <div className="d-flex-align-center">
            <PoweroffOutlined className="fs-16 mr-2" />
            <a onClick={this.handleLogout}>Đăng xuất</a>
          </div>
        </Menu.Item>
      </Menu>)
    return (
      <div className="header">
        <nav className="header__navbar" >
          <span className="header__navbar__toggle" id="js-navbar-toggle" onClick={this.showDrawerMenu}>
            <MenuUnfoldOutlined className="fs-24 cursor-pointer text-color" />
          </span>

          <div className="header__navbar__logo ">
            <Link href={`/`}>
              <img className="cursor-pointer" src={require("../assets/images/logo.jpeg")} />
            </Link>
          </div>
          <div className="h-100 d-flex-center-end">
            <ul className="header__navbar__main_nav h-100">
              <li>
                <Link href={`/`}>
                  <a className="nav_links">Trang chủ</a>
                </Link>
              </li>
              <li>
                <a href="#" className="nav_links">Blog</a>
              </li>
              {token ? (
                <div className="d-flex">
                  <li>
                    <Link href={`/host`}>
                      <a className="nav_links">Chủ nhà</a>
                    </Link>
                  </li>
                  <li>
                    <div className="ml-3 user_info d-flex-align-center">
                      <img src={require("../assets/images/logo.jpeg")} />
                      <Dropdown
                        style={{ width: "500px" }}
                        overlay={menuDropdown}
                        trigger={["click"]}
                      >
                        <a className="ml-3">
                          <span className="fs-16">{data && data.name}</span> <CaretDownFilled />
                        </a>
                      </Dropdown>
                    </div>
                  </li>
                </div>
              ) : (
                  <div className="d-flex">
                    <li>
                      <Link href={`/login`}>
                        <a className="nav_links">Đăng nhập</a>
                      </Link>
                    </li>
                    <li>
                      <Link href={`/register`}>
                        <a className="nav_links">Đăng ký</a>
                      </Link>
                    </li>
                  </div>
                )}
            </ul>
          </div >
          <span className="header__navbar__search">
            {visibleSearch ? <CloseOutlined className="fs-24 cursor-pointer text-color" onClick={this.onCloseSearch} /> : <SearchOutlined className="fs-24 cursor-pointer text-color" onClick={this.showDrawerSearch} />}
          </span></nav >
        <div className="mobile_menu" id="js-menu">
          <Drawer
            className="drawer_menu"
            title={
              <span onClick={this.onCloseMenu} >
                <CloseOutlined className="fs-24" />
              </span>
            }
            placement={placementMenu}
            closable={false}
            onClose={this.onCloseMenu}
            visible={visibleMenu}
            key={placementMenu}
          >
            <div className="main_nav">
              <ul>
                <li>
                  <Link href={`/`}>
                    <a className="nav_links">Trang chủ</a>
                  </Link>
                </li>
                <li>
                  <a href="#" className="nav_links">Blog</a>
                </li>
                {token ? (
                  <li>
                    <PoweroffOutlined className="fs-16 mr-2" />
                    <a onClick={this.handleLogout}>Đăng xuất</a>
                  </li>
                ) : (
                    <div className="d-flex-column">
                      <li>
                        <Link href={`/login`}>
                          <a className="nav_links">Đăng nhập</a>
                        </Link>
                      </li>
                      <li>
                        <Link href={`/register`}>
                          <a className="nav_links">Đăng ký</a>
                        </Link>
                      </li>
                    </div>
                  )}
              </ul>
            </div>
          </Drawer>
          <Drawer
            className={classnames('drawer_search', {
              'drawer_search__open': visibleSearch,
            })}
            placement={placementSearch}
            closable={false}
            onClose={this.onCloseSearch}
            visible={visibleSearch}
            key={placementSearch}
          >
            <SearchMobile  closeSearch={this.onCloseSearch}/>
          </Drawer>
        </div>

      </div >
    )
  }
}

const mapStateToProps = state => {
  return {
    userInfor: state.auths.userInfor,
  }
}

const mapDispatchToProps = dispatch => ({
  getUserInfor: () => dispatch(AuthsActions.getUserInfoRequest())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(HeaderComp))
