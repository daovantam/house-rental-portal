import React, { Component } from 'react'
import { connect } from 'react-redux'
import Router, { withRouter } from 'next/router'
import Head from 'next/head'
import LocationActions from '../../redux/_location-redux'
import CommonActions from '../../redux/_common-redux'
import Link from 'next/link'
import i18n from '../../i18n';
import { Select, Button, Input } from 'antd'
import { SearchOutlined, CloseOutlined, EnvironmentOutlined } from '@ant-design/icons';
import { MetaSearch } from '../../constants'
const { Option } = Select;
const priceList = MetaSearch.PRICE_LIST
const areaList = MetaSearch.AREA_LIST

class Search extends Component {
  state = {
    classNameSearchSlide: "slideClose mt-0",
    provinceId: "",
      districtId: "",
      wardId: "",
      price: "",
      acreage: "",
      categoryId: "",
      title: null
  }

  openSearchSlide = () => {
    this.setState({
      classNameSearchSlide: "slideOpen mt-2"
    })
  }

  closeSearchSlide = () => {
    this.setState({
      classNameSearchSlide: "slideClose mt-0"
    })
  }


  componentDidMount() {
    this.props.getListHouseCategory()
    let paramsProvince = {
      direction: false,
      matching: [],
      orderBy: "id",
      page: 0,
      pageSize: 100
    }
    this.props.getListProvince(paramsProvince)
  }
  handleSearchInput = (e) => {
    this.setState({
      title: e.target.value
    })
  }

  handleSelectProvince = (data) => {
    this.setState({
      provinceId: data
    })
    let paramsDistrict = {
      direction: false,
      matching: [
        {
          key: "provinceId",
          operation: ":",
          orPredicate: true,
          value: data
        }
      ],
      orderBy: "id",
      page: 0,
      pageSize: 100
    }
    this.props.getListDistrict(paramsDistrict)
  }

  handleSelectDistrict = (data) => {
    console.log(123)
    this.setState({
      districtId: data
    })
    let paramsWard = {
      direction: false,
      matching: [
        {
          key: "districtId",
          operation: ":",
          orPredicate: true,
          value: data
        }
      ],
      orderBy: "id",
      page: 0,
      pageSize: 100
    }
    this.props.getListWard(paramsWard)
  }

  handleSelectWard = (data) => {
    this.setState({
      wardId: data
    })
  }

  handleSelectPrice = (data) => {
    console.log(data)
    this.setState({
      price: data
    })
  }

  handleSelectAcreage = (data) => {
    this.setState({
      acreage: data
    })
  }

  handleSelectCategoryId = (data) => {
    this.setState({
      categoryId: data
    })
  }

  searchHouse = () => {
    let searchQuery = new URLSearchParams()
    let matching = []
    if (this.state.title) {
      searchQuery.set('title', this.state.title)
    } else searchQuery.delete('title')
    if (this.state.provinceId) {
      searchQuery.set('provinceId', this.state.provinceId)
    } else searchQuery.delete('provinceId')
    if (this.state.districtId) {
      console.log(this.state.districtId)
      searchQuery.set('districtId', this.state.districtId)
      console.log(searchQuery.toString())
    } else searchQuery.delete('districtId')
    if (this.state.wardId) {
      searchQuery.set('wardId', this.state.wardId)
    } else searchQuery.delete('districtId')
    if (this.state.categoryId) {
      searchQuery.set('categoryId', this.state.categoryId)
    } else searchQuery.delete('categoryId')
    if (this.state.price) {
      MetaSearch.PRICE_LIST.map((item) => {
        if (this.state.price === item.id) {
          searchQuery.set('price_range', item.id)
          searchQuery.set('from_price', item.from)
          searchQuery.set('to_price', item.to)
        }
      })
    } else {
      searchQuery.delete('price_range')
      searchQuery.delete('from_price')
      searchQuery.delete('to_price')
    }
    if (this.state.acreage) {
      MetaSearch.AREA_LIST.map((item) => {
        if (this.state.acreage === item.id) {
          searchQuery.set('acreage_range', item.id)
          searchQuery.set('from_acreage', item.from)
          searchQuery.set('to_acreage', item.to)
        }
      })
    } else {
      searchQuery.delete('acreage_range')
      searchQuery.delete('from_acreage')
      searchQuery.delete('to_acreage')
    }
    console.log(searchQuery.toString())
    Router.push(`/search?${searchQuery.toString()}`).then(() => window.scrollTo(0, 0))
  }

  render() {
    const { listProvince, listDistrict, listWard, listHouseCategory } = this.props

    return (
      <div>
        <div className="home_page__search_container__search_bar mt-2">
          <div className="d-flex home_page__search_container__search_input">
            <Input prefix={<EnvironmentOutlined className="fs-16" />} placeholder="Tìm kiếm địa điểm, khu vực" bordered={false} onFocus={this.openSearchSlide} onChange={this.handleSearchInput} />
            <Button size="middle" type="primary" style={{ width: 250, height: '48px' }} onClick={() => { this.searchHouse() }}>
              <SearchOutlined /> <span className="fw-7 fs-16">Tìm kiếm</span>
            </Button>
          </div>
          <div className={`${this.state.classNameSearchSlide}`}>
            <div className="text-right">
              <CloseOutlined className="cursor-pointer color-white fs-16" onClick={this.closeSearchSlide} />
            </div>
            <div className="d-flext-space-between mt-2">
              <Select
                showSearch
                className="search_item ml-0"
                size="large"
                placeholder="Tỉnh/thành phố"
                optionFilterProp="children"
                onSelect={this.handleSelectProvince}
                filterOption={(input, option) =>
                  option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
              >
                {listProvince && listProvince.map((item, index) => {
                  return (
                    <Option value={item.id} key={index}>{item.name}</Option>
                  )
                })}
              </Select>
              <Select
                showSearch
                className="search_item"
                size="large"
                placeholder="Quận/huyện"
                optionFilterProp="children"
                onSelect={this.handleSelectDistrict}
                filterOption={(input, option) =>
                  option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
              >
                {listDistrict && listDistrict.map((item, index) => {
                  return (
                    <Option value={item.id} key={index}>{item.name}</Option>
                  )
                })}
              </Select>
              <Select
                showSearch
                className="search_item mr-0"
                size="large"
                placeholder="Phường/xã"
                optionFilterProp="children"
                onSelect={this.handleSelectWard}
                filterOption={(input, option) =>
                  option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
              >
                {listWard && listWard.map((item, index) => {
                  return (
                    <Option value={item.id} key={index}>{item.name}</Option>
                  )
                })}
              </Select>

            </div>
            <div className="d-flext-space-between mt-4">
              <Select
                showSearch
                className="search_item ml-0"
                size="large"
                placeholder="Mức giá"
                optionFilterProp="children"
                onSelect={this.handleSelectPrice}
                filterOption={(input, option) =>
                  option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
              >
                {
                  priceList.map((item, index) => {
                    return (
                      <Option value={item.id} key={index}>{item.name}</Option>
                    )
                  })
                }
              </Select>
              <Select
                showSearch
                className="search_item"
                size="large"
                placeholder="Diện tích"
                optionFilterProp="children"
                onSelect={this.handleSelectAcreage}
                filterOption={(input, option) =>
                  option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
              >
                {
                  areaList.map((item, index) => {
                    return (
                      <Option value={item.id} key={index}>{item.name}</Option>
                    )
                  })
                }
              </Select>
              <Select
                showSearch
                className="search_item mr-0"
                size="large"
                placeholder="Kiểu chỗ ở"
                optionFilterProp="children"
                onSelect={this.handleSelectCategoryId}
                filterOption={(input, option) =>
                  option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
              >
                {
                  listHouseCategory && listHouseCategory.map((item, index) => {
                    return (
                      <Option value={item.id} key={index}>{item.name}</Option>
                    )
                  })
                }
              </Select>

            </div>

          </div>
        </div>

      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    listProvince: state.location.listProvince,
    listDistrict: state.location.listDistrict,
    listWard: state.location.listWard,
    listHouseCategory: state.common.listHouseCategories,
  }
}

const mapDispatchToProps = dispatch => ({
  getListProvince: data => dispatch(LocationActions.getProvinceRequest(data)),
  getListDistrict: data => dispatch(LocationActions.getDistrictRequest(data)),
  getListWard: data => dispatch(LocationActions.getWardRequest(data)),
  getListHouseCategory: () => dispatch(CommonActions.getHouseCategoryRequest()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Search))
