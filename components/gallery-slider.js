import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Slider from 'react-slick'
import classnames from 'classnames'
import { LightgalleryProvider, LightgalleryItem } from 'react-lightgallery'


// const { photos } = props
const settings = {
  infinite: false,
  slidesToShow: 3,
  variableWidth: true,
  speed: 800,
  swipe: false,
  responsive: [
    {
      breakpoint: 767,
      settings: {
        infinite: true,
        slidesToShow: 1,
        speed: 400,
        swipe: true,
      }
    },

  ]
}
class GallerySlider extends Component {


  handleHover = (key) => {
    // setHoverItem(key)
  }

  render() {
    const { photo } = this.props

    return (
      <div
        className={classnames(
          'space-detail__gallery')
        }
      >
        <LightgalleryProvider
          lightgallerySettings={
            {
              download: false,
            }
          }
        >
          <Slider {...settings}>
            {photo && photo.map((val, key) => {
              return (
                <div key={key} className="space-detail__gallery-image">
                  <LightgalleryItem src={val} group="space">
                    <img src={val}
                      onMouseEnter={this.handleHover(key)}
                      onMouseLeave={this.handleHover(key)}
                    />
                  </LightgalleryItem>
                </div>
              )
            })}
          </Slider>
        </LightgalleryProvider>
      </div>
    )
  }
}

GallerySlider.propTypes = {
  photos: PropTypes.array
}

export default GallerySlider
