import React, { Component } from 'react'
import { Tag } from 'antd';
import {
  PhoneOutlined,
  MailOutlined,
  TwitterOutlined,
  FacebookOutlined
} from '@ant-design/icons';
class Footer extends Component {
  render() {
    return (
      <div className="ft_container">
        <div className="ft_container__main">
          <div className="ft_container__item">
            <h2 className="ft_container__item--title">Không gian ưa thích</h2>
            <ul>
              <li><a href="#">Chung cư mini</a></li>
              <li><a href="#">Nhà riêng</a></li>
              <li><a href="#">Homestay</a></li>
              <li><a href="#">Studio</a></li>
            </ul>
          </div>
          <div className="ft_container__item">
            <h2 className="ft_container__item--title">Hỗ trợ</h2>
            <ul>
              <li><a href="#">Trở thành đối tác</a></li>
              <li><a href="#">Sales</a></li>
              <li><a href="#">Quảng cáo</a></li>
            </ul>
          </div>
          <div className="ft_container__item">
            <h2 className="ft_container__item--title">Liên hệ</h2>
            <div>
              <PhoneOutlined className="main-icon mr-2" />
              <span className="room_detail__content__right__content-text">Điện thoại</span>
            </div>
            <ul className="ml-4">
              <li><a href="#">+84 6666 8888</a></li>
            </ul>
            <div>
              <MailOutlined className="main-icon mr-2" />
              <span className="room_detail__content__right__content-text">Email</span>
            </div>
            <ul className="ml-4">
              <li><a href="#">houserental@gmail.com</a></li>
            </ul>
          </div>
          <div className="ft_container__item">
            <div className="d-flex">
              <h2 className="ft_container__item--title">House Rental</h2>
              <div className="ft_container__item__logo">
                <img src={require("../assets/images/logo.jpeg")} />
              </div>
            </div>
            <p>Subscribe to our newsletter to get our latest news.</p>
            <Tag icon={<TwitterOutlined />} color="#55acee">
              Twitter
            </Tag>
            <Tag icon={<FacebookOutlined />} color="#3b5999">
              Facebook
            </Tag>
          </div>
        </div>

        <div className="ft_container__legal">
          <span>&copy; 2020 Copyright House Rental.</span>
        </div>
      </div>
    )
  }
}

export default Footer