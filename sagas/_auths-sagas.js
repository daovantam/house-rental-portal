/**
 * @author Nam NH
 * TodoSagas
 */

import { put, call } from 'redux-saga/effects'

import { authsService } from '../services'
import AuthsActions from '../redux/_auths-redux'
import { push } from 'react-router-redux';
import { HttpStatus } from '../constants'
import { Cookies } from 'react-cookie';
const cookies = new Cookies();

const AuthsSagas = {
  *login({ data }) {
    let response = yield call(authsService.login, data)
    let responsedata = yield response.json()
    if (response.status < HttpStatus.BAD_REQUEST) {
      responsedata.login = true
      window.localStorage.setItem('jwt_auth_token', responsedata.access_token)
      cookies.set('jwt_auth_token', responsedata.access_token);
      yield put(AuthsActions.authsSuccess(responsedata))
    } else {
      yield put(AuthsActions.authsFailure(responsedata, response.status))
    }
  },

  *register({ data }) {
    let response = yield call(authsService.register, data.params)
    let responsedata = yield response.json()
    if (response.status < HttpStatus.BAD_REQUEST) {
      responsedata.register = true
      yield put(AuthsActions.authsSuccess(responsedata))
      yield data.callback(true);
    } else {
      yield put(AuthsActions.authsFailure(responsedata, response.status))
    }
  },

  *forgotPassword({ data }) {
    let response = yield call(authsService.forgotPassword, data)
    let responsedata = yield response.json()
    if (response.status < HttpStatus.BAD_REQUEST) {
      responsedata.forgotPassword = true
      yield put(AuthsActions.authsSuccess(responsedata))
    } else {
      yield put(AuthsActions.authsFailure(responsedata, response.status))
    }
  },

  *resetPassword({ data }) {
    let response = yield call(authsService.resetPassword, data)
    let responsedata = yield response.json()
    if (response.status < HttpStatus.BAD_REQUEST) {
      responsedata.resetPassword = true
      yield put(AuthsActions.resetPasswordSuccess(responsedata))
    } else {
      yield put(AuthsActions.resetPasswordFailure(responsedata, response.status))
    }
  },

  *socialLogin({ data }) {
    let response = yield call(authsService.socialLogin, data)
    let responsedata = yield response.json()
    if (response.status < HttpStatus.BAD_REQUEST) {
      responsedata.socialLogin = true
      yield put(AuthsActions.authsSuccess(responsedata))
    } else {
      yield put(AuthsActions.authsFailure(responsedata, response.status))
    }
  },

  *getUserInfor({ data }) {
    let response = yield call(authsService.userInfo, data)
    let responsedata = yield response.json()
    if (response.status < HttpStatus.BAD_REQUEST) {
      responsedata.getUserInfor = true
      yield put(AuthsActions.getUserInfoSuccess(responsedata))
    } else {
      yield put(AuthsActions.getUserInfoFailure(responsedata, response.status))
    }
  }
}

export default AuthsSagas