/**
 * @author Nam NH
 * TodoSagas
 */

import { put, call } from 'redux-saga/effects'

import { locationService } from '../services'
import LocationActions from '../redux/_location-redux'
import { HttpStatus } from '../constants'

const LocationSagas = {
  *getListProvince({ data }) {
    let response = yield call(locationService.getProvince, data)
    let responsedata = yield response.json()
    if (response.status < HttpStatus.BAD_REQUEST) {
      responsedata.getListProvince = true
      yield put(LocationActions.getProvinceSuccess(responsedata))
    } else {
      yield put(LocationActions.getProvinceFailure(responsedata, response.status))
    }
  },

  *getListDistrict({ data }) {
    let response = yield call(locationService.getDistrict, data)
    let responsedata = yield response.json()
    if (response.status < HttpStatus.BAD_REQUEST) {
      responsedata.getListDistrict = true
      yield put(LocationActions.getDistrictSuccess(responsedata))
    } else {
      yield put(LocationActions.getDistrictFailure(responsedata, response.status))
    }
  },

  *getListWard({ data }) {
    let response = yield call(locationService.getWard, data)
    let responsedata = yield response.json()
    if (response.status < HttpStatus.BAD_REQUEST) {
      responsedata.search = true
      yield put(LocationActions.getWardSuccess(responsedata))
    } else {
      yield put(LocationActions.getWardFailure(responsedata, response.status))
    }
  }

   
}

export default LocationSagas