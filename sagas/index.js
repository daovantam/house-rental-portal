/**
 * @author NamNH
 * Saga index: connects action type and saga
 */

import { takeLatest, all } from 'redux-saga/effects'

/* ------------- Types ------------- */
import { AuthsTypes } from '../redux/_auths-redux'
import { HouseTypes } from '../redux/_house-redux'
import { LocationTypes } from '../redux/_location-redux'
import { CommonTypes } from '../redux/_common-redux'
/* ------------- Sagas ------------- */
import ErrorsSagas from './_errors-sagas'
import AuthsSagas from './_auths-sagas'
import HouseSagas from './_house-sagas'
import LocationSagas from './_location-sagas'
import CommonSagas from './_common-sagas'
/* ------------- Connect Types To Sagas ------------- */
export default function* root() {
    yield all([
        //common
        takeLatest(CommonTypes.GET_HOUSE_CATEGORY_REQUEST, CommonSagas.getHouseCategory),


        //authentication
        takeLatest(AuthsTypes.LOGIN_REQUEST, AuthsSagas.login),
        takeLatest(AuthsTypes.REGISTER_REQUEST, AuthsSagas.register),
        takeLatest(AuthsTypes.FORGOT_PASSWORD_REQUEST, AuthsSagas.forgotPassword),
        takeLatest(AuthsTypes.RESET_PASSWORD_REQUEST, AuthsSagas.resetPassword),
        takeLatest(AuthsTypes.SOCIAL_LOGIN_REQUEST, AuthsSagas.socialLogin),
        takeLatest(AuthsTypes.GET_USER_INFO_REQUEST, AuthsSagas.getUserInfor),
        takeLatest(AuthsTypes.AUTHS_FAILURE, ErrorsSagas.raiseError),

        //house
        takeLatest(HouseTypes.SEARCH_REQUEST, HouseSagas.search),
        takeLatest(HouseTypes.GET_DETAIL_HOUSE_REQUEST, HouseSagas.getDetailHouse),
        takeLatest(HouseTypes.GET_TOP_HOUSE_REQUEST, HouseSagas.getTopHouse),
        takeLatest(HouseTypes.CREATE_HOUSE_REQUEST, HouseSagas.createHouse),

        //location
        takeLatest(LocationTypes.GET_PROVINCE_REQUEST, LocationSagas.getListProvince),
        takeLatest(LocationTypes.GET_DISTRICT_REQUEST, LocationSagas.getListDistrict),
        takeLatest(LocationTypes.GET_WARD_REQUEST, LocationSagas.getListWard),

    ])
}