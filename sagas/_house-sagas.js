/**
 * @author Nam NH
 * TodoSagas
 */

import { put, call } from 'redux-saga/effects'

import { houseService } from '../services'
import HouseActions from '../redux/_house-redux'
import { HttpStatus } from '../constants'
import { message } from "antd";

const HouseSagas = {
  *search({ data }) {
    let response = yield call(houseService.search, data)
    let responsedata = yield response.json()
    if (response.status < HttpStatus.BAD_REQUEST) {
      responsedata.search = true
      yield put(HouseActions.searchSuccess(responsedata))
    } else {
      yield put(HouseActions.searchFailure(responsedata, response.status))
    }
  },

  *getDetailHouse({ data }) {
    let response = yield call(houseService.getDetailHouse, data)
    let responsedata = yield response.json()
    if (response.status < HttpStatus.BAD_REQUEST) {
      responsedata.getDetailHouse = true
      yield put(HouseActions.getDetailHouseSuccess(responsedata))
    } else {
      yield put(HouseActions.getDetailHouseFailure(responsedata, response.status))
    }
  },

  *getTopHouse({ data }) {
    let response = yield call(houseService.search, data)
    let responsedata = yield response.json()
    if (response.status < HttpStatus.BAD_REQUEST) {
      responsedata.search = true
      yield put(HouseActions.getTopHouseSuccess(responsedata))
    } else {
      yield put(HouseActions.getTopHouseFailure(responsedata, response.status))
    }
  },

  *createHouse({ data }) {
    let response = yield call(houseService.createNewHouse, data)
    let responsedata = yield response.json()
    if (response.status < HttpStatus.BAD_REQUEST) {
      responsedata.search = true
      yield put(HouseActions.createHouseSuccess(responsedata))
      message.success("Đăng phòng thành công", 3);
    } else {
      yield put(HouseActions.createHouseFailure(responsedata, response.status))
    }
  }

   
}

export default HouseSagas