/**
 * @author Nam NH
 * TodoSagas
 */

import { put, call } from 'redux-saga/effects'

import { commonService } from '../services'
import CommonActions from '../redux/_common-redux'
import { HttpStatus } from '../constants'

const CommonSagas = {
  *getHouseCategory() {
    let response = yield call(commonService.getListHouseCategory)
    let responsedata = yield response.json()
    if (response.status < HttpStatus.BAD_REQUEST) {
      responsedata.getHouseCategory = true
      yield put(CommonActions.getHouseCategorySuccess(responsedata))
    } else {
      yield put(CommonActions.getHouseCategoryFailure(responsedata, response.status))
    }
  },
}

export default CommonSagas