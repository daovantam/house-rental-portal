import { createReducer, createActions } from 'reduxsauce'

/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({
  getProvinceRequest: ['data'],
  getProvinceSuccess: ['data'],
  getProvinceFailure: ['error', 'status'],
  getDistrictRequest: ['data'],
  getDistrictSuccess: ['data'],
  getDistrictFailure: ['error', 'status'],
  getWardRequest: ['data'],
  getWardSuccess: ['data'],
  getWardFailure: ['error', 'status'],
})

export const LocationTypes = Types
export default Creators

/* ------------- Initial State ------------- */
export const INITIAL_STATE = {
  processing: false,
  succeed: false,
  listProvince: [],
  listDistrict: [],
  listWard: []
}

/* ------------- Reducers ------------- */
export const request = state => {
  return { ...state, processing: true }
}

export const getProvinceSuccess = (state, { data }) => {
  return { ...state, processing: false, succeed: true, listProvince: data.data.list_object }
}

export const getDistrictSuccess = (state, { data }) => {
  console.log(data)
  return { ...state, processing: false, succeed: true, listDistrict: data.data.list_object }
}

export const getWardSuccess = (state, { data }) => {
  return { ...state, processing: false, succeed: true, listWard: data.data.list_object }
}

export const failure = (state) => {
  return { ...state, processing: false }
}

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_PROVINCE_REQUEST]: request,
  [Types.GET_PROVINCE_SUCCESS]: getProvinceSuccess,
  [Types.GET_PROVINCE_FAILURE]: failure,
  [Types.GET_DISTRICT_REQUEST]: request,
  [Types.GET_DISTRICT_SUCCESS]: getDistrictSuccess,
  [Types.GET_DISTRICT_FAILURE]: failure,
  [Types.GET_WARD_REQUEST]: request,
  [Types.GET_WARD_SUCCESS]: getWardSuccess,
  [Types.GET_WARD_FAILURE]: failure
})
