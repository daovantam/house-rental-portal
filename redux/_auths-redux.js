import { createReducer, createActions } from 'reduxsauce'

/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({
  loginRequest: ['data'],
  registerRequest: ['data'],
  forgotPasswordRequest: ['data'],
  socialLoginRequest: ['data'],
  getUserInfoRequest: ['data'],
  getUserInfoSuccess: ['data'],
  getUserInfoFailure: ['error', 'status'],
  authsSuccess: ['data'],
  authsFailure: ['error', 'status'],
  resetPasswordRequest: ['data'],
  resetPasswordSuccess: ['data'],
  resetPasswordFailure: ['error', 'status'],
})

export const AuthsTypes = Types
export default Creators

/* ------------- Initial State ------------- */
export const INITIAL_STATE = {
  processing: false,
  succeed: false,
  data: {},
  userInfor: {}
}

/* ------------- Reducers ------------- */
export const request = state => {
  return { ...state, processing: true }
}

export const success = (state, { data }) => {
  return { ...state, processing: false, succeed: true, data }
}

export const getUserInforSuccess = (state, { data }) => {
  return { ...state, processing: false, succeed: true, userInfor: data }
}

export const failure = (state) => {
  return { ...state, processing: false }
}

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
  [Types.LOGIN_REQUEST]: request,
  [Types.REGISTER_REQUEST]: request,
  [Types.FORGOT_PASSWORD_REQUEST]: request,
  [Types.SOCIAL_LOGIN_REQUEST]: request,
  [Types.GET_USER_INFO_REQUEST]: request,
  [Types.GET_USER_INFO_SUCCESS]: getUserInforSuccess,
  [Types.GET_USER_INFO_FAILURE]: failure,
  [Types.AUTHS_SUCCESS]: success,
  [Types.AUTHS_FAILURE]: failure,
  [Types.RESET_PASSWORD_REQUEST]: request,
  [Types.RESET_PASSWORD_SUCCESS]: success,
  [Types.RESET_PASSWORD_FAILURE]: failure
})
