import { createReducer, createActions } from 'reduxsauce'

/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({
  searchRequest: ['data'],
  searchSuccess: ['data'],
  searchFailure: ['error', 'status'],
  getTopHouseRequest: ['data'],
  getTopHouseSuccess: ['data'],
  getTopHouseFailure: ['error', 'status'],
  getDetailHouseRequest: ['data'],
  getDetailHouseSuccess: ['data'],
  getDetailHouseFailure: ['error', 'status'],
  createHouseRequest: ['data'],
  createHouseSuccess: ['data'],
  createHouseFailure: ['error', 'status']
})

export const HouseTypes = Types
export default Creators

/* ------------- Initial State ------------- */
export const INITIAL_STATE = {
  processing: false,
  succeed: false,
  listTopHouse: {},
  searchData: {},
  detailData: {}
}

/* ------------- Reducers ------------- */
export const request = state => {
  return { ...state, processing: true }
}

export const getTopHouseSuccess = (state, { data }) => {
  return { ...state, processing: false, succeed: true, listTopHouse: data.data }
}

export const searchSuccess = (state, { data }) => {
  return { ...state, processing: false, succeed: true, searchData: data.data }
}

export const getDetailSuccess = (state, { data }) => {
  return { ...state, processing: false, succeed: true, detailData: data.data }
}

export const createHouseSuccess = (state, { data }) => {
  return { ...state, processing: false}
}

export const failure = (state) => {
  return { ...state, processing: false }
}

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
  [Types.SEARCH_REQUEST]: request,
  [Types.SEARCH_SUCCESS]: searchSuccess,
  [Types.SEARCH_FAILURE]: failure,
  [Types.GET_DETAIL_HOUSE_REQUEST]: request,
  [Types.GET_DETAIL_HOUSE_SUCCESS]: getDetailSuccess,
  [Types.GET_DETAIL_HOUSE_FAILURE]: failure,
  [Types.GET_TOP_HOUSE_REQUEST]: request,
  [Types.GET_TOP_HOUSE_SUCCESS]: getTopHouseSuccess,
  [Types.GET_TOP_HOUSE_FAILURE]: failure,
  [Types.CREATE_HOUSE_REQUEST]: request,
  [Types.CREATE_HOUSE_SUCCESS]: createHouseSuccess,
  [Types.CREATE_HOUSE_FAILURE]: failure
})
