import { createReducer, createActions } from 'reduxsauce'

/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({
  getHouseCategoryRequest: ['data'],
  getHouseCategorySuccess: ['data'],
  getHouseCategoryFailure: ['error', 'status']
})

export const CommonTypes = Types
export default Creators

/* ------------- Initial State ------------- */
export const INITIAL_STATE = {
  processing: false,
  succeed: false,
  listHouseCategories: []
}

/* ------------- Reducers ------------- */
export const request = state => {
  return { ...state, processing: true }
}

export const abc = (state, { data }) => {
  return { ...state, processing: false, succeed: true, listHouseCategories: data.data }
}

export const failure = (state) => {
  return { ...state, processing: false }
}

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_HOUSE_CATEGORY_REQUEST]: request,
  [Types.GET_HOUSE_CATEGORY_SUCCESS]: abc,
  [Types.GET_HOUSE_CATEGORY_FAILURE]: failure
})
