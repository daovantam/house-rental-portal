/**
 * @author Nam NH
 * This file combines all reducers and create redux store
 */

import { combineReducers } from 'redux'
import { reducer as modal } from 'redux-modal'

const rootReducer = combineReducers({
  modal,
  auths: require('./_auths-redux').reducer,
  houses: require('./_house-redux').reducer,
  location: require('./_location-redux').reducer,
  common: require('./_common-redux').reducer,
  errors: require('./_errors-redux').reducer,
})

export default rootReducer