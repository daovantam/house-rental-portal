/**
 * @author Nam NH
 * ApiClient to interact with api server
 */

import fetch from "isomorphic-unfetch";
import QueryString from "query-string";
import getConfig from "next/config";
import {Auth} from '../constants'
import { Cookies } from 'react-cookie';
const reactCookies = new Cookies();
export default class Api {
  constructor(defaultConfig) {
    this.defaultConfig = defaultConfig;
  }

  request(url, cookies) {
    const { serverRuntimeConfig } = getConfig();
    // let prefix = serverRuntimeConfig.onServer
    //   ? process.env.REACT_APP_BASE_URL + "/api"
    //   : "/api";
    let prefix = process.env.REACT_APP_BASE_URL;
    // if (cookies && cookies.token) {
    //   this.defaultConfig.headers['Authorization'] = `bearer ${cookies.token}`;
    // } else {
    //   delete this.defaultConfig.headers['Authorization']
    // }
    let token = reactCookies.get('jwt_auth_token')
    if (token) {
      this.defaultConfig.headers['Authorization'] = `bearer ${token}`;
    } else {
      delete this.defaultConfig.headers['Authorization']
    }
    return fetch(prefix + url, this.defaultConfig);
  }

  get(url, data = null, cookies) {
    this.defaultConfig.method = "GET";
    this.defaultConfig.body = undefined;
    this.defaultConfig.headers['Content-Type'] = 'application/json'
    // if (data) {
    //   url += "?" + QueryString.stringify(data);
    // }
    return this.request(url, cookies);
  }

  post(url, data, api_type) {
    this.defaultConfig.method = "POST";
    if (!(data instanceof FormData)) {
      data = JSON.stringify(data)
      this.defaultConfig.headers['Content-Type'] = 'application/json'
    } else {
      // this.defaultConfig.headers['Content-Type'] = 'multipart/form-data'
      delete this.defaultConfig.headers['Content-Type']
    }
    if(api_type === 'login') {
      this.defaultConfig.headers['Authorization'] = "Basic Auth" + btoa(Auth.USER_NAME + ":" + Auth.PASSWORD)
    }
    this.defaultConfig.body = data;
    return this.request(url);
  }

  put(url, data) {
    this.defaultConfig.method = "PUT";
    if (FormData && data instanceof FormData) {
      delete this.defaultConfig.headers["Content-Type"]      
    } else {
      data = JSON.stringify(data);
      this.defaultConfig.headers["Content-Type"] = "application/json";
    }
    this.defaultConfig.body = data;
    return this.request(url);
  }

  patch(url, data) {
    this.defaultConfig.method = "PATCH";
    if (FormData && data instanceof FormData) {
      delete this.defaultConfig.headers["Content-Type"]      
    } else {
      data = JSON.stringify(data);
      this.defaultConfig.headers["Content-Type"] = "application/json";
    }
    this.defaultConfig.body = data;
    return this.request(url);
  }

  delete(url) {
    this.defaultConfig.method = "DELETE";
    return this.request(url);
  }
}
