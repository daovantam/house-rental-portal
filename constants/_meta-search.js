export default class MetaSearch {
  static PRICE_LIST = [
    { id: 1, name: 'Dưới 2 triệu', from: 0, to: 2000000 },
    { id: 2, name: '2 - 5 triệu', from: 2000000, to: 5000000 },
    { id: 3, name: '5 - 10 triệu', from: 5000000, to: 10000000 },
    { id: 4, name: 'Trên 10 triệu', from: 10000000, to: '' },
  ]
  static AREA_LIST = [
    { id: 1, name: 'Dưới 20 m2', from: 0, to: 20 },
    { id: 2, name: '20 m2 - 50 m2', from: 20, to: 50 },
    { id: 3, name: '50 m2 - 100 m2', from: 50, to: 100 },
    { id: 4, name: 'Lớn hơn 100 m2', from: 100, to: '' },
  ]
}
