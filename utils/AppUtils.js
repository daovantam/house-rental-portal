export default class AppUtils {
  static number_format( num) {
    if (num) {
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    } else return 0
  }
}