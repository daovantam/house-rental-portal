import Cookies from 'js-cookie'

export default class AuthsUtils {
  static isAuthenticated() {
    return !!Cookies.get('jwt_auth_token')
  }

  static login() {
    Cookies.set('jwt_auth_token', true, { expires: 1 })
  }

  static logout() {
    Cookies.remove('jwt_auth_token')
  }
}
